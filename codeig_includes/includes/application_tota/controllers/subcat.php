<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class subcat extends CI_Controller 
{
	function index($id = null)
	{		
		if(!preg_match("/\d+\.(html|json)/i", $id))
		{
			show_error('Sub category doesn\'t exist', 404);
			return;
		}
		$ajax = preg_match("/\d+\.json/i", $id);

		$this->load->model('subcat_model');		
		
		$data = $this->subcat_model->GetArticles($id);
		if(!$data)
		{
			show_error('No articles', 404);
			return;
		}
		if(!$ajax)
			$this->load->view('header', $data);
		$this->load->view('subcat_view', $data);		
		if(!$ajax)
			$this->load->view('footer', $data);		
	}
	
}
