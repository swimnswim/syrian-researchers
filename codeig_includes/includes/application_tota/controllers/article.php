<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Article extends CI_Controller 
{
	function index($id = null)
	{		
		$ajax = preg_match("/\d+\.json/i", $id);
		if(!preg_match("/\d+\.html/i", $id) && !preg_match("/\d+\.json/i", $id))
		{
			show_error('Article doesn\'t exist', 404);
			return;
		}
		$this->load->model('cat_model');		
		$this->load->model('article_model');
		
		$data['article'] = $this->article_model->GetById($id);
		if($data['article']['youtube_id'])
		{
			$this->load->view('video_view', $data);	
			return;			
		}
		if(!$data['article'])
		{
			show_error('Article doesn\'t exist', 404);
			return;
		}		
		$cats = $this->cat_model->GetAll();	
		$data['signatures'] = $this->article_model->getSignatures($id);	
		
		if(!$ajax)
			$this->load->view('header', $data);		
		
		
		$this->load->view('article_view', $data);	
		
		if(!$ajax)
			$this->load->view('footer', $data);		
	}
	function Related($id = null)
	{
		$this->load->model('article_model');
		echo json_encode($this->article_model->getRelatedArticles($id));	
	}
	function downloadStr($url)
	{
		$ch = curl_init();
		$timeout = 25;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$data = curl_exec($ch);
		return $data;
	}
	function validate($return = 0, $uid = null)
	{
		if(!$uid)
			$uid = $this->input->post('unique_id');
			
		$user = $this->downloadStr("https://graph.facebook.com/v2.3/me?access_token=$uid");		
		$user = json_decode($user, true);
		$this->load->model('article_model');
		if($return)
			return $this->article_model->validate_id($user['id']);	
		else
			echo $this->article_model->validate_id($user['id']);
	}
	function update($article_id = null)
	{
		if(!$article_id) return;
		
		$id = $this->validate(1);
		
		if(!$id) {echo "Access denied" ; return;}
		
		$this->load->model('user_model');
		$this->load->model('article_model');
		
		$user = $this->user_model->getByFBID($id);
		$admins = $this->article_model->getArticleAdmins($article_id);
		$admin_id = $admins[0]->user_id;
		$token = uniqid(md5(rand(1,5)));
		
		$data = array(
		'new_title' => $this->input->post('new_title'),
		'new_body' => $this->input->post('new_body'),
		'notes' => $this->input->post('notes'),
		'type' => $this->input->post('Reason'),
		'crucial' => $this->input->post('crucial') == 'on' ? 1 : 0,
		'article_id' => $article_id,
		'token_user_id' => $admin_id,
		'token' => $token,
		'editor_id' => $user['id']);		
		
		$edit_id = $this->article_model->InsertEdit($data);	

		$data = urlencode("$edit_id|$token");
		foreach($admins as $admin)
		{
			$this->user_model->Notify("Hello ". $admin->adminname ."! Someone edited you article, please take a look", $admin->fb_id, "edits/view_edit_token.php?data=$data");
		}	
		echo "Thanks!<script>setTimeout(\"window.location='../$article_id.html'\", 2000);</script>";
	}
	function edit($id = null)
	{		
		$ajax = preg_match("/\d+\.json/i", $id);
		if(!preg_match("/\d+\.html/i", $id) && !preg_match("/\d+\.json/i", $id))
		{
			show_error('Article doesn\'t exist', 404);
			return;
		}
		$this->load->model('cat_model');		
		$this->load->model('article_model');
		
		$data['article'] = $this->article_model->GetById($id, 1);
		if($data['article']['youtube_id'])
		{
			$this->load->view('video_view', $data);	
			return;			
		}
		if(!$data['article'])
		{
			show_error('Article doesn\'t exist', 404);
			return;
		}		
		$cats = $this->cat_model->GetAll();	
		$data['signatures'] = $this->article_model->getSignatures($id);	
		
		if(!$ajax)
			$this->load->view('header', $data);		
		
		
		$this->load->view('article_edit_view', $data);	
		
		if(!$ajax)
			$this->load->view('footer', $data);		
	}
	function rate($id)
	{
		if(!$id) return;
		
		$rating = json_decode($this->input->post('rating'), true);
		$rating['article_id'] = $id;

		unset($rating['done']);
		$token = $rating['token'];	
		
		$id = $this->validate(1, $token);		
		
		if(!$id) {echo "Access denied" ; return;}
		
		$this->load->model('user_model');
		
		$user = $this->user_model->getByFBID($id);
		$rating['user_id'] = $user['id'];
		
		unset($rating['token']);		
		$this->load->model('article_model');
		$this->article_model->Rate($rating); 
	}
	function read_it($id)
	{
		$this->load->model('article_model');
		$this->article_model->Increment($id,1);
		return;
	}
	function view_it($id)
	{
		$this->load->model('article_model');
		$this->article_model->Increment($id);
		return;
	}
	
	
}