<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class home extends CI_Controller 
{
	function index()
	{		
		$data['is_home'] = 1;
		$this->load->view('header', $data);		
		$this->load->model('home_model');				
		$data['covers'] = $this->home_model->GetCoverArticles();		
		$data['recent_arts'] = $this->home_model->GetRecentArticles();		
		$data['top_read'] = $this->home_model->GetMostReadArticles();				
		$data['news'] = $this->home_model->GetNews();				
		$this->load->view('home_view', $data);			
		$this->load->view('footer');		
	}
	function ajax()
	{	
		$this->load->model('home_model');				
		$data['recent_arts'] = $this->home_model->GetRecentArticles();		
		$data['top_read'] = $this->home_model->GetMostReadArticles();	
		$data['covers'] = $this->home_model->GetCoverArticles();		
		$data['news'] = $this->home_model->GetNews();				
		$this->load->view('home_view', $data);			
	}
	
}