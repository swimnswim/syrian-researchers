<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller 
{
	function index($id = null)
	{		
		if(!preg_match("/\d+\.(html|json)/i", $id))
		{
			show_error('User doesn\'t exist', 404);
			return;
		}
		$ajax = preg_match("/\d+\.json/i", $id);
		
		$this->load->model('user_model');		

		
		$data['user'] = $this->user_model->GetById($id);
		if(!$data['user'])
		{
			show_error('User doesn\'t exist', 404);
			return;
		}		
		if(!$ajax)
			$this->load->view('header', $data);		
		
		$this->load->view('user_view', $data);
		
		if(!$ajax)
			$this->load->view('footer', $data);		
	}
	
}