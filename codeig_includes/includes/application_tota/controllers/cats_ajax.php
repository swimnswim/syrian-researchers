<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cats_ajax extends CI_Controller 
{
	function index()
	{
		$this->load->model('cat_model');
		$cats = $this->cat_model->GetAll();	
		echo json_encode($cats);
	}
	
}