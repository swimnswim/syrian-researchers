<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends CI_Controller 
{
	function index($name2 = null)
	{		
		if($name2 == null) return;
		$name = "static_pages/".str_replace(".json", ".html", basename($name2));
		if(!file_exists($name))
		{
			show_error('Page doesn\'t exist', 404);
			return;
		}
		
		$ajax = pathinfo(basename($name2), PATHINFO_EXTENSION) == 'json';
		
		$data['html'] = file_get_contents($name);
		if(!$ajax)
			$this->load->view('header');
			
		$this->load->view('static_page', $data);
		
		if(!$ajax)
			$this->load->view('footer');
		}
	
}