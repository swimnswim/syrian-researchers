<?php
class subcat_model extends CI_Model
{
	function GetArticles($id)
	{
		$this->load->database();
		$result = new stdClass();
		$arts = array();
		$query = $this->db->order_by('publish_date', 'DESC')->get_where('article', array('article.sub_cat_id' => $id, 'deleted'=> 0));
		foreach ($query->result() as $row)
		{
			$row->views = $row->desktop_views + $row->mob_views + $row->app_views;
			$arts[] = $row;			
		}	
		//get cat info
		$this->db->select("u1,u2,u3,u4, sub_cat.name as subcatname, category.name as catname, sub_cat.id as subcatid, category.id as catid");
		$this->db->from('sub_cat');
		$this->db->join('category', 'sub_cat.mother_cat = category.id');
		$this->db->join('cat_and_admin', 'cat_and_admin.cat_id = category.id');
		$this->db->where(array('sub_cat.id' => $id));
		$cat = $this->db->get()->row_array();

		//get admins info
		$this->db->select("name, id, fb_id");
		$this->db->from("user");
		$this->db->where("id", $cat['u1']);
		$this->db->or_where("id", $cat['u2']);
		$this->db->or_where("id", $cat['u3']);
		$this->db->or_where("id", $cat['u4']);
		
		$admins_rs = $this->db->get();
		$admins = array();
		foreach ($admins_rs->result() as $row)
		{
			$admins[] = $row;
		}		
		
		//send the data
		if(count($arts) == 0) return null;
		$query = $this->db->get_where('sub_cat', array('id' => $id));	
		$result->arts = $arts;
		$result->admins = $admins;
		$result->cat = $cat;
		return $result;
	}
}