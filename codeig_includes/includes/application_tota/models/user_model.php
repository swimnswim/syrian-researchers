<?php
class user_model extends CI_Model
{
	function GetById($id)
	{
		//user info
		$this->load->database();
		$this->db->select("id, name, fb_id, user.views as uviews, desc, email, twitter, gender");
		$this->db->from('user');		
		$this->db->where(array('user.id' => $id));
		$result = $this->db->get();
		$user = $result->row_array();
		
		
		// cats they worked
		$this->db->select("category.name as catname");
		$this->db->from('signature');		
		$this->db->join('article', 'signature.article_id = article.id');
		$this->db->join('category', 'article.cat_id = category.id');
		$this->db->group_by("category.id");
		$this->db->having("count('signature.id') > 5");
		$this->db->where(array('signature.user_id' => $id, 'article.deleted' => '0'));
		
		$result = $this->db->get();
		$cats = array();
		foreach($result->result() as $row)
			$cats[] = $row->catname;
		
		
		//articles worked on
		$this->db->select("title, article.id as article_id");
		$this->db->from('signature');		
		$this->db->join('article', 'signature.article_id = article.id'); 
		$this->db->where(array('signature.user_id' => $id, 'article.deleted' => '0'));
		$this->db->group_by("article_id"); 
		
		$result = $this->db->get();
		$arts = array();
		foreach($result->result() as $row)
			$arts[] = $row;
			
		//collect data
		$user['arts'] = $arts;
		$user['cats'] = $cats;
		if(!$user) return null;
		return $user;
	}
	function Notify($msg, $userid, $href)
	{
		$ch = curl_init();
		$msg = urlencode($msg);
		$token = "496625687093064|DgNpYWhFVvlHdbVWKYrceDTN2rw";
		$token = urlencode($token);
		$postData = "href=$href&template=$msg&access_token=$token";
		$url = "https://graph.facebook.com/v2.2/$userid/notifications";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);
	}
	function NotifyAdmin($article_id, $edit_id, $token)
	{
		$article = getArticleById($article_id);
		$admins = GetCatAdmins($article['cat_id']);
		
		$data = urlencode("$edit_id|$token");
		foreach($admins as $admin)
		{
			Notify("Hello ". $admin['name']."! Someone edited you article(".$article['title']."), please take a look", $admin['fb_id'], "edits/view_edit_token.php?data=$data");
		}
	}
	function getByFBID($fbid)
	{
		$result = $this->db->get_where('user', array('fb_id' => $fbid));
		$user = $result->row_array();
		return $user;
	}
	function getSignatures($id)
	{
		$this->load->database();

		$this->db->select("name, label");
		$this->db->from('signature');
		$this->db->join('user', 'signature.user_id = user.id');	
		$this->db->where(array('signature.article_id' => $id));
		$result = $this->db->get();
		$signatures = array();
		foreach($result->result() as $row)
			$signatures[] = $row;
		return $signatures;
	}
}
