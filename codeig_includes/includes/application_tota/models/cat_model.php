<?php
class cat_model extends CI_Model
{
	function GetAll()
	{
		$this->load->database();
		$result = array();
		$query = $this->db->order_by('ord', 'asc')->get('category');
		$index = 0;
		foreach ($query->result() as $row)
		{
			$result[$index] = $row;
			$squery = $this->db->get_where('sub_cat', array('mother_cat' => $row->id));
			$result[$index]->subcats = array();
			foreach ($squery->result() as $srow)
			{
				$result[$index]->subcats[] = $srow;	
			}
			$index++;
		}
		return $result;
	}
}