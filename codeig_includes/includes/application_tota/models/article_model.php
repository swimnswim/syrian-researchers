<?php
class article_model extends CI_Model
{
	function GetById($id, $edit = 0)
	{
		
		$this->load->database();

		$this->db->select("article.title, article.id as article_id, sub_cat.id as subcatid, replace(fbid, '_', '/posts/') as fbid, youtube_id,  DATE_FORMAT(`article`.`publish_date`, '%d-%m-%Y') as publish_date, (desktop_views+mob_views+app_views) as `views`, article.body, category.name as catname, sub_cat.name as subcatname, html, article.home_pic_id, article.sub_cat_id, article.cat_id, picture.width as picwidth, picture.height as picheight, picture.path as picpath", false);
		$this->db->from('article');
		$this->db->join('category', 'article.cat_id = category.id');
		$this->db->join('sub_cat', 'article.sub_cat_id = sub_cat.id');
		$this->db->join('picture', 'article.home_pic_id = picture.id');
		
		$this->db->where(array('article.id' => $id));
		$result = $this->db->get();
		$art = $result->row_array();
		if(!$art) return null;
		
		if(!$edit)
			$art['body'] = ParseImages(ParseVideos(makeClickableLinks(DashesToHR($art['body']))));
		else
		{
			$art['body'] = preg_replace('(\n|\r)', '', $art['body']);
			$art['body'] = str_replace("<br />", "\n", $art['body']);
		}
			
		$art['is_infog'] = $art['cat_id'] == 1;
		return $art;
	}
	function getRelatedArticles($cat_id)
	{
		$this->load->database();
		$articles = Array();
	
		$this->db->select("article.title, article.id as article_id, article.home_pic_id, picture.width as picwidth, picture.height as picheight, picture.path as picpath", false);
		$this->db->from('article');
		$this->db->join('picture', 'article.home_pic_id = picture.id');
		$this->db->where(array('article.cat_id' => $cat_id, 'picture.width' => 1000, 'picture.height' => 1000));
		$this->db->group_by("article_id")->order_by('article_id', 'random')->limit(5);
		$result = $this->db->get();
		
		foreach($result->result() as $row)
			$articles[] = $row;
		return $articles;
	}
	function Update($data)
	{
		$this->load->database();
		$this->db->db_debug = FALSE;
		$this->db->insert('rating', $obj);
		if($this->db->_error_message()) echo 0; else echo 1;
	}	
	function validate_id($id)
	{	
		$this->load->database();
		$result = $this->db->get_where('user', array('fb_id' => $id));
		$user = $result->row_array();
		
		if($user)
			return $user['fb_id'];
		else
			return 0;
	}
	function getArticleAdmins($id)
	{
		$this->load->database();

		$this->db->select("user.name as adminname, user.id as user_id, fb_id");
		$this->db->from('article');
		$this->db->join('category', 'article.cat_id = category.id');
		$this->db->join('cat_and_admin', 'cat_and_admin.cat_id = category.id');
		$this->db->join('user', 'user.id = u1 or user.id = u2 or user.id = u3 or user.id = u4');
		$this->db->where(array('article.id' => $id));
		$result = $this->db->get();
		$admins = array();
		foreach($result->result() as $row)
			$admins[] = $row;
		return $admins;	
	}
	function InsertEdit($data)
	{
		$this->load->database();
		$this->db->insert('edits', $data);
		return $this->db->insert_id();
	}
	function Increment($id, $read = 0)
	{
		$this->load->database();
		if($read)
			$this->db->query("update `article` set `reads` = `reads` + 1 where `id` = '$id'");
		else
			$this->db->query("update `article` set `desktop_views` = `desktop_views` + 1 where `id` = '$id'");			
	}
	function Rate($obj)
	{	
		$this->load->database();
		$this->db->db_debug = FALSE;
		$this->db->insert('rating', $obj);
		if($this->db->_error_message()) echo 0; else echo 1;
	}	
	function getSignatures($id)
	{
		$this->load->database();

		$this->db->select("name, label, user_id");
		$this->db->from('signature');
		$this->db->join('user', 'signature.user_id = user.id');	
		$this->db->where(array('signature.article_id' => $id));
		$result = $this->db->get();
		$signatures = array();
		foreach($result->result() as $row)
			$signatures[] = $row;
		return $signatures;
	}
}
