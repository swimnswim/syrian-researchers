<?php
class home_model extends CI_Model
{
	function GetCoverArticles()
	{
		$this->load->database();
		$arts = array();
		$this->db->select("article.title, article.id as article_id, fb_share_count, publish_date, (desktop_views+mob_views+app_views) as `views`, article.body, category.name as catname, sub_cat.name as subcatname, html, article.home_pic_id, article.sub_cat_id, article.cat_id, picture.width as picwidth, picture.height as picheight, picture.path as picpath", false);
		$this->db->from('article');
		$this->db->join('category', 'article.cat_id = category.id');
		$this->db->join('sub_cat', 'article.sub_cat_id = sub_cat.id');
		$this->db->join('picture', 'article.home_pic_id = picture.id');	
		
		$this->db->order_by('article.id DESC')->where(array('article.publish_date <' => date("Y-m-d H:i:s"), 'deleted' => 0, 'picture.width' => 1000, 'picture.height' => 1000))->limit(100);
		$result = $this->db->get();
		
		foreach ($result->result() as $row)
		{
			$arts[] = $row;			
		}		
		return $arts;	
	}
	function GetRecentArticles()
	{
		$this->load->database();
		$arts = array();
		$this->db->select("article.title, article.id as article_id, fb_share_count, publish_date, (desktop_views+mob_views+app_views) as `views`, article.body, category.name as catname, sub_cat.name as subcatname, html, article.home_pic_id, article.sub_cat_id, article.cat_id, picture.width as picwidth, picture.height as picheight, picture.path as picpath", false);
		$this->db->from('article');
		$this->db->join('category', 'article.cat_id = category.id');
		$this->db->join('sub_cat', 'article.sub_cat_id = sub_cat.id');
		$this->db->join('picture', 'article.home_pic_id = picture.id');	
		
		$this->db->order_by('article.id DESC')->where(array('article.publish_date <' => date("Y-m-d H:i:s"), 'deleted' => 0))->limit(9);
		$result = $this->db->get();
		
		foreach ($result->result() as $row)
		{
			$arts[] = $row;			
		}		
		return $arts;
	}
	function GetMostReadArticles()
	{
		$this->load->database();
		$arts = array();
		$this->db->select("article.title, article.id as article_id, fb_share_count, publish_date, (desktop_views+mob_views+app_views) as `views`, article.body, category.name as catname, sub_cat.name as subcatname, html, article.home_pic_id, article.sub_cat_id, article.cat_id, picture.width as picwidth, picture.height as picheight, picture.path as picpath", false);
		$this->db->from('article');
		$this->db->join('category', 'article.cat_id = category.id');
		$this->db->join('sub_cat', 'article.sub_cat_id = sub_cat.id');
		$this->db->join('picture', 'article.home_pic_id = picture.id');	
		
		$this->db->order_by('views DESC')->where(array('article.publish_date <' => date("Y-m-d H:i:s"), 'deleted' => 0))->limit(10);
		$result = $this->db->get();
		
		foreach ($result->result() as $row)
		{
			$arts[] = $row;			
		}		
		return $arts;
	}
	function GetNews()
	{
		$this->load->database();
		$arts = array();
		$this->db->select("article.title, article.publish_date, id as article_id", false);
		$this->db->from('article');
		
		$this->db->order_by('id DESC')->where(array('article.publish_date <' => date("Y-m-d H:i:s"), 'deleted' => 0))->limit(10);
		$result = $this->db->get();
		
		foreach ($result->result() as $row)
		{
			$arts[] = $row;			
		}		
		return $arts;
	}
}