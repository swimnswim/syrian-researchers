<?php
function makeClickableLinks($text)
	{
		return preg_replace_callback(
				'#https?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#',
				function ($matches)
				{
					$m = $matches[0];
					$url = $m;
					$m = str_replace("http://", "", $m);
					$m = str_replace("https://", "", $m);
					$m = str_replace("www.", "", $m);
					if(strlen($m) > 30)
					{
						$m = substr($m, 0, 15) . "..." . substr($m, -15);
					}
					return "<a style='font-weight: bold; color: #23B0FC' target=_blank href='$url'>هنا</a>";
				}				
				,
				$text
		);
	}
	function FixNumbers($text)
	{
		return preg_replace_callback(
				"/\s\d+/",
				function ($matches)
				{
					$m = $matches[0];
					return "<span class='number'>$m</span>";
				}				
				,
				$text
		);
	}
	function FixEnglishAndNumbers($text)
	{
		return FixNumbers(FixEnglishChars($text));
	}
	function FixEnglishChars($text)
	{
		return preg_replace_callback(
				"/([a-zA-Z]+\s?)+/i",
				
				function ($matches)
				{
					$m = $matches[0];
					return "<span class='number'>$m</span>";
				}
				
				,
				$text
		);
	}
	
	
	function ParseImages($text)
	{
		return preg_replace_callback('/\[{4}img:\d+\]{4}/i', 
				function ($matches)
				{
					$images = $matches;
					foreach($images as $img)
					{
						$id = substr($img, 8);
						$id = substr($id, 0, strpos($id, ']'));
						$pic = GetPic($id);
						$title = $pic['alt'];
				
						return "<a target='_blank' href='http://www.syr-res.com/pic_ret.php?id=$id&b=1'><img title='$title' class='double-border' style='max-width: ".$pic['width']."px; width: 100%;' src='http://www.syr-res.com/pic_ret.php?id=$id&b=1'></a>";
					}
				}
				
				, $text);
	}
	function DashesToHR($text)
	{
		return preg_replace('/(\-{10,})?(\_{10,})/', "</p><hr /><p style='text-align: justify'>", $text);
	}
	function GetPic($id)
	{
		$CI =& get_instance();
		
		$CI ->db->select("*");		
		$CI ->db->from('picture');
		$CI ->db->where(array('id' => $id));
		$result = $CI->db->get();
		$art = $result->row_array();
		
		return $art;
	}
	function ParseVideos($text)
	{
		return preg_replace_callback('/\[{4}vid:[^&\n]+\]{4}/i', 
				
				function ($matches)
				{
					$images = $matches;
					foreach($images as $img)
					{
						$id = substr($img, 8);
						$id = substr($id, 0, strpos($id, ']'));
						$id = trim($id);
						return "<iframe class='yt_iframe' width='280' height='200' src='https://www.youtube.com/embed/$id?feature=player_embedded&theme=light' frameborder='0' allowfullscreen></iframe>";
					}
				}				
				, $text);
	}
	?>