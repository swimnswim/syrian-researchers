<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['subcat/(:any)'] = "subcat/index/$1";
$route['article/(:num).html'] = "article/index/$1.html";
$route['article/R(:num).html'] = "article/index/$1.html";
$route['article/M(:num).html'] = "article/index/$1.html";
$route['article/(:num).json'] = "article/index/$1.json";

$route['article/read/(:num).json'] = "article/read_it/$1";
$route['article/view/(:num).json'] = "article/view_it/$1";
$route['article/related/(:num).json'] = "article/Related/$1";
$route['article/rate/(:num).json'] = "article/rate/$1";
$route['article/update/(:num).html'] = "article/update/$1";

$route['pages/(:any)'] = "pages/index/$1";
$route['article_ajax/(:any)'] = "article_ajax/index/$1";
$route['user/(:any)'] = "user/index/$1";
$route['ajax'] = "home/ajax";

$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
