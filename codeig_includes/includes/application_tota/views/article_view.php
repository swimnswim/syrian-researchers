		<div id="edit-div"><a>Edit</a> | <span onclick="$(this).parent().hide('slow')">x<span></div>
		<article>
			<div class="container article-container">
		        <h1 id='article-title'><?php echo FixEnglishAndNumbers($article['title'])?></h1>
		        <h3 class="cat-name" id='cat-name-h3'><span style='color:#444'><?php echo $article['catname'] . " >>>> </span><a data-asset='/subcat/{$article['subcatid']}.html' href='../subcat/{$article['subcatid']}.html'>" . $article['subcatname'] ?></a></h3>
		        	<p class="article-text">
					<div>
						<img id='article-img' style="<?php if($article['is_infog']) echo 'margin: 5px; float: none; max-width:'.$article['picwidth'].'px"'?>" class="article-img" data-src="http://www.syr-res.com/pic_ret.php?b=1&id=<?php echo $article['home_pic_id']?>"/>
					</div>
					<main id='article-html'><?php echo $article['body']?></main>
				</p> 
				<div id="end-div" class="clearfix"></div>
				<hr>
				<div id='fb-like-btn'>
					<div class="fb-like" data-href="<?php echo $this->config->item('base_url')."article/".$article['article_id'].".html"?>" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
				</div>
				<div class="share-buttons-div">
					<div class="share-buttons-title">						
						<h3><span class="glyphicon glyphicon-share"></span> شارك </h3>
					</div>
					<div>
						<div  id="wa-btn" class="share-buttons share-buttons-wa">						
							<a href="whatsapp://send?text=من الباحثون السوريون: http://www.syr-res.com/article/<?php echo $article['article_id']?>.html" data-text="مقال حلو من الباحثون السوريون:"><img src='../images/whatsapp.png'></a></span>
						</div><div id="pdf-share-btn" class="share-buttons share-buttons-pdf">
                                                        <a href="/pdf.php?id=<?php echo $article['article_id']?>"><img src='../images/pdf.png'></a>
                                                </div><div id="gp-share-btn" class="share-buttons share-buttons-gplus">						
							<span class="share-genericon genericon-googleplus"></span>
						</div><div id='fb-share-btn' class="share-buttons share-buttons-fb">						
							<span class="share-genericon genericon-facebook"></span>
						</div>
					</div>
					<div>
						<div id="pin-share-btn" class="share-buttons share-buttons-pin">			
							<span class="share-genericon genericon-pinterest"></span>
						</div><div class="share-buttons share-buttons-pocket">		
							<div id='pocket-scrollable'>
								<span class="share-genericon genericon-pocket"></span>	
								<div id='pocket-iframe'>
									<a data-pocket-label="pocket" data-pocket-count="none" class="pocket-btn" data-lang="en"></a>
									<script type="text/javascript">!function(d,i){if(!d.getElementById(i)){var j=d.createElement("script");j.id=i;j.src="https://widgets.getpocket.com/v1/j/btn.js?v=1";var w=d.getElementById(i);d.body.appendChild(j);}}(document,"pocket-btn-js");</script>
								</div>
							</div>
						</div><div id="tweet-share-btn" class="share-buttons share-buttons-twitter">						
							<span class="share-genericon genericon-twitter"></span>
						</div>
					</div>
				</div>
				<div class="share-buttons-div">
					<div class="share-buttons-title">						
						<h3><span class="glyphicon glyphicon-th-list"></span> تفاصيل</h3>
					</div><div class="details-buttons details-buttons-date">						
						<span class="glyphicon glyphicon-calendar"></span> 
						<span id='article-date'><?php echo $article['publish_date'] ?></span>
					</div><div class="details-buttons details-buttons-views">						
						<span class="glyphicon glyphicon-eye-open"></span> 
						<span id='article-views'><?php echo $article['views'] ?></span>
					</div><?php if($article['fbid']) {?><div class="share-buttons details-buttons-original-post">		
						<a target='_blank' href="<?php echo "https://www.facebook.com/".$article['fbid']?>">
							البوست
						</a>
					</div>
					<?php } ?>
				</div>
				<div class="signatures-buttons-div">
					<div class="signatures-buttons-title">						
						<h3><span class="glyphicon glyphicon-th-list"></span> المساهمون في الإعداد</h3>
					</div>	
					<span id='article-signatures'>
					<?php
						foreach($signatures as $s)
						{
							echo '<div class="details-buttons details-buttons-signature">'.$s->label.' :<a data-asset="'.$this->config->item('base_url').'user/'.$s->user_id.'.json" href="'.$this->config->item('base_url').'user/'.$s->user_id.'.html">'. $s->name.'</a></div>';
						}
					?>	
					</span>
				</div>
				<div class="clearfix"></div>
				<div id="quality-div" class="rating-stars-div">
					<div class="signatures-buttons-title">						
						<h3><span class="glyphicon glyphicon glyphicon-star"></span> مراقبة الجودة</h3>
					</div>	
					<span id='article-signatures'>
						<div class="details-buttons details-buttons-signature"><span class='rating-label'>الإملاء: </span>
							<div class="rating" id="spelling">
								<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
							</div>						
						</div>
						<div class="details-buttons details-buttons-signature"><span class='rating-label'>الدقة العلمية: </span>
							<div class="rating" id="accuracy">
								<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
							</div>						
						</div>
						<div class="details-buttons details-buttons-signature"><span class='rating-label'>اللغة: </span>
							<div class="rating" id="language">
								<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
							</div>						
						</div>
						<div class="details-buttons details-buttons-signature"><span class='rating-label'>الصياغة: </span>
							<div class="rating" id="writing">
								<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
							</div>						
						</div>
						<div id="rating-button" class="details-buttons details-buttons-signature">
							<button id="submit-rating" value="إرسال التقييم">إرسال التقييم</button>
						</div>
						
					</span>
				</div>
				<div class="clearfix"></div>
				<div id="comments-section">
					<hr>
					<div class="fb-comments" id='comments-fbml' data-baseurl="<?php echo $this->config->item('base_url')?>" data-href="<?php echo $this->config->item('base_url')."article/".$article['article_id'].".html"?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
					<div style="height: 100px"></div>
				</div>
			</div>
	</article>
	<script>
		var isInfog = <?php echo $article['is_infog'] ? 'true' : 'false'?>;
		var article_id = <?php echo $article['article_id']?>;
		var aspectRatio = <?php echo $article['picheight'] / $article['picwidth']?>;
		var cat_id =  <?php echo $article['cat_id']?>;
	</script>
	<script src="<?php echo $this->config->item('base_url')?>js/article.js"></script>
