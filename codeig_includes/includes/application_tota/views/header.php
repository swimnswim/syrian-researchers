<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta property="fb:app_id" content="496625687093064"/>
<meta property="fb:admins" content="100000238097307"/>
<meta name="google-site-verification" content="k9PoxBBb2G6dx52qF4WIH7BcEyjhlLZJ_sI5MBYHuHA"/>
<?php
	if(isset($article)) 
	{
		
		$hint = $article['body'];
		$hint = substr($hint, 0, 300);
		$hint = str_replace("\r", "", $hint);
		$hint = str_replace("\n", " ", $hint);
		$hint .= "...";
		
		echo '
		<meta name="description" content="'.htmlspecialchars($hint).'"/> 
		<meta property="og:url" content="http://www.syr-res.com/article/'. $article['article_id'].'.html"/>
		<meta property="og:title" content="الباحثون السوريون - '. $article['title']. '"/>
		<meta property="og:image" content="http://www.syr-res.com/pictures/'. $article['picpath'].'"/>
		<meta property="og:type" content="article">
		<meta property="og:locale" content="ar_AR">
		<link rel="canonical" href="http://www.syr-res.com/article/'. $article['article_id'].'.html">
		<meta property="article:published_time" content="'. $article['publish_date'].'">';
		foreach($signatures as $s)
		{
			echo '<meta property="article:author" content="http://www.syr-res.com/user/'. $s->user_id .'.html">';
		}
		echo '
		<meta property="article:section" content="'. $article['catname'].'">
		';
	}
	else if(isset($user)) 
	{
		$name = explode(' ', $user['name']);
		echo '
		<meta property="og:url" content="http://www.syr-res.com/user/'. $user['id'].'.html"/>
		<meta property="og:title" content="الباحثون السوريون - '. $user['name']. '"/>
		<meta property="og:image" content="http://www.syr-res.com/team_pics/get.php?id='. $user['fb_id'].'"/>
		<meta property="og:type" content="profile">
		<meta property="og:locale" content="ar_AR">
		<link rel="canonical" href="http://www.syr-res.com/user/'. $user['id'].'.html">
		<meta property="og:image:width" content="50">
		<meta property="og:image:height" content="50">
		<meta property="og:image:type" content="image/png">
		<meta property="profile:first_name" content="'.$name[0].'">
		<meta property="profile:last_name" content="'.$name[count($name) - 1].'">
		<meta property="profile:gender" content="'.$user['gender'].'">
		<meta property="profile:username" content="johndoe">
		';
	}
	else echo '
		<meta property="og:url" content="http://www.syr-res.com/"/>
		<meta property="og:title" content="الباحثون السوريون"/>
		<meta property="og:image" content="http://www.syr-res.com/images/nfb-logow.jpg"/>
		<meta property="og:type"   content="website" /> 
		<meta property="og:descrition" content="مبادرة أطلقها الباحث السوري الدكتور مهند ملك بالتعاون مع بعض الشابات والشباب السوريين المقيمين في سورية والمغتربين إضافة إلى مجموعة من الشباب العربي الراقي"/>
	';
	
?>
<link rel="icon" type="image/png" href="/favico.png" />

<title>Syrian Researchers</title>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.min.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link href="http://www.owlcarousel.owlgraphic.com/assets/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,700italic,400,600'	rel='stylesheet' type='text/css'>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.cycle2.js"></script>
<script src="<?php echo $this->config->item('base_url')?>js/main.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url')?>newcss/main.css"/>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url')?>newcss/glyphs.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div id="fb-root"></div>
<div id="related-articles">
	<div class="container">		
		<div class="related-carousel" style="width: 100%; overflow: hidden">			
			<div id='related-holder'>
				<h4 style="margin:5px; width: 100%;">مواضيع مرتبطة
					<p id='related-more'>المزيد &gt;</p>
					<div class="clearfix"></div>
				</h4>
			</div>
			
		</div>
	</div>
</div>
</head>
<nav id="color-switch-navbar"
	class="navbar navbar-inverse navbar-static-top transparent-black-bg"
	role="navigation">
	<div class="container" id="top-nav-bar">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div>
					<div id="header-logo-and-text">					
						<div style="float: right">
							<img id="logo-img" src="<?php echo $this->config->item('base_url')?>images/top-logo.png" />
						</div>
						<div class="vcenter">
							<p class="our_font">سنعيد كتابة العلم بأبجدية عربية</p>
						</div>	
						<div class="social">
							<a target='_blank' href="https://www.facebook.com/Syrian.researchers"><span class="genericon genericon-facebook"></span></a>
							<a target='_blank' href="https://www.linkedin.com/company/syrian-researchers"><span class="genericon genericon-linkedin-alt"></span></a>
							<a target='_blank' href="https://www.youtube.com/user/syrianRes"><span class="genericon genericon-youtube"></span></a>
							<a target='_blank' href="https://twitter.com/res_syr"><span class="genericon genericon-twitter"></span></a>
							<a target='_blank' href="https://instagram.com/syrian_researchers/"><span class="genericon genericon-instagram"></span></a>
						</div>							
					</div>
			</div>
		</div>
	</div>
	<div id="menu-big-container">
		<div class="container" id="menu-big-bar">
				<div id="collapse-menu-btn" onclick="showMenu()">
					<span class="glyphicon glyphicon-align-justify"></span>
				</div>
			<div id="menu-bar">
				<ul id="menu-list" >
					<li><a href='<?php echo $this->config->item('base_url')?>' data-asset='<?php echo $this->config->item('base_url')?>ajax'>الرئيسية</a></li><li 
onclick="showCatsMenu()"><a class="hint--bottom" data-hint="يمكنك عرض فئات الموقع من هنا">الفئات</a><span class="caret"></span></li><li><a target="_blank" href="/TV/">الباحثون 
السوريون 
TV</a></li><!--<li><a href='<?php echo $this->config->item('base_url')?>pages/inter.html' data-asset='<?php echo $this->config->item('base_url')?>pages/inter.json'>القسم التفاعلي</a></li>--><li><a href='<?php echo $this->config->item('base_url')?>pages/about.html' data-asset='<?php echo $this->config->item('base_url')?>pages/about.json'>من نحن</a></li><li><a href='<?php echo $this->config->item('base_url')?>pages/contact.html' data-asset='<?php echo $this->config->item('base_url')?>pages/contact.json'>اتصل بنا</a></li>
					<li style="float: left;">
						<span style="display: inline-block; width: 280px; color: #000">
							<gcse:search></gcse:search>
						</span>
					</li>					
				</ul>
			</div>			
		</div>
	</div>
	<div id="cats-menu" style="position: absolute; width:100%; background: rgba(0,0,0,0.9); color: #fff">	
			<div class="menu-container-fluid masonry">
			<div id="close-button" onclick="close_cats_menu()">
			x
			</div>
			<div id="cats_div">
				<div id="cats-loading-div">جارِ تحميل الفئات</div>
			</div>
			</div>
	</div>
</nav>
<body>
<div id="loading-div"></div>
<script>
var home_url = "<?php echo $this->config->item('base_url')?>";
(function() {
var cx = '008190865264526382200:puftbhwfij4';
var gcse = document.createElement('script');
gcse.type = 'text/javascript';
gcse.async = true;
gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
   '//www.google.com/cse/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(gcse, s);
})();
<?php
if(!$_COOKIE['seen_cats'])
echo "notifyCats();";
?>
</script>
<section id='main-section'>
