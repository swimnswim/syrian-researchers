	</section>	
	<footer>
		<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
		<div class="container">
			<div class="col-md-6">
				<h3>عائلتنا</h3>
				<div>
					<a target="_blank" href="https://www.facebook.com/Arts.Workshop?fref=ts"><img src='<?php echo $this->config->item('base_url')?>images/arts_workshop.png'></a>
				</div>
				<div>
					<a target="_blank" href="https://www.facebook.com/Syr.Res.Tech?fref=ts"><img src='<?php echo $this->config->item('base_url')?>images/syrres_tech.png'></a>
				</div>
			</div>
			<div class="col-md-6">
				<ul id='footer-links'>
					
				</ul>
			</div>
			<div class="clearfix"></div>
			<div id="footer-signature">
			جميع الحقوق محفوظة لمبادرة "الباحثون السوريون" - 2015
			</div>
		</div>
	</footer>
	</body>
</div>
