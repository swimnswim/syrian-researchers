<section id="carousel-section">
	<div id="carousel-div">
		<div id="overflower">
			<?php 
			foreach($covers as $c)
			{
				
				echo "<div data-id='{$c->article_id}' class='banner-item'><a data-asset='{$this->config->item('base_url')}article/{$c->article_id}.json' href='{$this->config->item('base_url')}article/{$c->article_id}.html'><img data-src='http://www.syr-res.com/pictures/{$c->picpath}'></a></div>";
			}
			?>
		</div>
		<div id="controls">
			<a id="right-arrow"><span id='right-arr' style="color: #fff" class="banner-genericon genericon-rightarrow"></span></a>
			<a id="left-arrow"><span class="banner-genericon genericon-leftarrow"></span></a>
					
		</div>
	</div>
</section>
<section id="team-section">
	<div id='team-container' class="container">
		<h1>قصة نجاح</h1>
	</div>
</section>
<section id="stats-section">
	<div class="col-md-3">
		<p><span class="home-stats-genericon genericon-heart home-stats-genericon-pink"></span>		
		<h3>أكثر من مليون زائر شهرياً</h3>
	</div>
	<div class="col-md-3">
		<p><span class="home-stats-genericon genericon-info home-stats-genericon-blue"></span>
		<h3>ما يقارب ستة آلاف موضوع علمي </h3>
	</div>
	<div class="col-md-3">
		<p><span class="home-stats-genericon genericon-help home-stats-genericon-fushi"></span>
		<h3>مئة سؤال يومي من متابعينا</h3>
	</div>
	<div class="col-md-3">
		<p><span class="home-stats-genericon genericon-user"></span>
		<h3>أكثر من 250 متطوعاً</h3>
	</div>
	
	<div class="clearfix"></div>
	

</section>
<section id="almashy">
	<div class="container">
		<h1>ابقوا معنا</h1>
	</div>
	<div class="container stay-wth-us-div">
		<div class="stay-wth-us">
			<p><a target='_blank' href="https://instagram.com/syrian_researchers/"><span class="hoverable home-stats-genericon genericon-instagram 
home-stats-genericon-fushi"></span></a>
		</div>
		<div class="stay-wth-us">
			<p><a target='_blank' href="https://www.facebook.com/Syrian.researchers?fref=ts"><span class="hoverable home-stats-genericon genericon-facebook home-stats-genericon-blue"></span></a>
		</div>
		<div class="stay-wth-us">
			<p><a target='_blank' href="https://www.youtube.com/user/syrianRes"><span class="hoverable home-stats-genericon genericon-youtube home-stats-genericon-youtube"></span></a>
		</div>
		<div class="stay-wth-us">
			<p><a target='_blank' href="https://www.linkedin.com/company/syrian-researchers"><span class="hoverable home-stats-genericon genericon-linkedin"></span></a>
		</div>		
		<div class="stay-wth-us">
			<p><a target='_blank' href="http://www.syr-res.com/rss.php" class="hoverable home-stats-genericon-rss icon-large"><i class="icon-large icon-rss home-stats-genericon-fushi"></i></span></a>
		</div>
		<div class="stay-wth-us">
			<p><a target='_blank' href="https://twitter.com/res_syr"><span class="hoverable home-stats-genericon genericon-twitter home-stats-genericon-twitter"></span></a>
		</div>
		<div class="stay-wth-us">
			<p><a target='_blank' href="https://play.google.com/store/apps/details?id=omer.ahmed.syrianresearchers&hl=en"><img src='images/play-store.png' class="hoverable" style="width: 90px; margin-top: 20px; position: relative"></span></a>
		</div>
	</div>
	<div class="clearfix"></div>
</section>
<section id="browse-section">
	<div>
		<h1><a href="#" onclick="showCatsMenu();">تصفح موقعنا</a></h1>
	</div>
</div>
<script>
	if($(window).width() > 600)
	{
		$("#carousel-section").show();
		var currentMargin = 0;
		var elementsShown = Math.floor($(window).width() / 500);
		var allElements = $('#overflower').children().length - 2;
		var page = 0;
		$('#carousel-div').width(elementsShown * 500);
		var maxMargin = (allElements - elementsShown) * 500;
		var pages = Math.floor(allElements / elementsShown);
		$('.banner-item img').each(function(i, e)
		{
			if(i < elementsShown)
				$(this).attr('src', $(this).attr('data-src'));	
		});		
		$(document).ready(function()
		{
			$('#overflower').width($('#overflower').children().length * 500);	
			$('#overflower').css('display', 'block');
			
			$('#left-arrow').click(function()
			{
				if(page + 1 >= pages) return;
				page++;
				for(var i = page * elementsShown; i < (page * elementsShown + elementsShown); i++)
				{
					var item = $('#overflower').children()[i];
					var img = $(item).find('img');
					img.attr('src', img.attr('data-src'));	
				}
				currentMargin -= 500 * elementsShown;			
				$('#overflower').css('margin-right', currentMargin + 'px');
				$('#right-arr').css('color', '#333');
			});
			$('#right-arrow').click(function()
			{
				if(page == 0) return;
				page--;
				for(var i = page * elementsShown; i < (page * elementsShown + elementsShown); i++)
				{					
					var item = $('#overflower').children()[i];
					var img = $(item).find('img');
					img.attr('src', img.attr('data-src'));	
				}
				currentMargin += 500 * elementsShown;			
				$('#overflower').css('margin-right', currentMargin + 'px');	

			});
			
		});
	}
</script>
