		<script>
		window.fbAsyncInit = function()
		{
				FB.init({
				appId      : '496625687093064',
				xfbml      : true,
				version    : 'v2.3'
			});
			function LoginToFB()
			{
				FB.login(handler);
			}
			handler = function(response) {
				// Here we specify what we do with the response anytime this event occurs. 
				if (response.status === 'connected')
				{
					FB.api("/me", function(res)	
					{ 		
						$.post("<?php echo $this->config->item('base_url') ."article/validate/"?>", "unique_id="+FB.getAccessToken(), function(data)
						{
						if(data == res.id)
						{
							$('#edit-main-div').show();
							$('#logging-in').hide();
							$('#unique_id').val(FB.getAccessToken());
							var ta = document.getElementById('edit_ta');
							ta.style.height = ta.scrollHeight + 100 + "px";
						}
						else
						{
							$('#logging-in').html('<h3>فشل تسجيل الدخول</h3>');
						}
						}
					);
					}
					);
				}
				else if (response.status === 'not_authorized')
				{
					$('#logging-in').html('<h3>Identity check failed</h3>');
				}
				else
				{
					$('#logging-in').html('<h3><a href="#b" onclick="LoginToFB()">You have to login first</a></h3>');	
				}
			};
			FB.getLoginStatus(handler);
		}
		</script>
		<article>
			<div id="logging-in" class="container"><h3>جاري تسجيل الدخول</h3></div>
			<div id="edit-main-div" class="container">
			 <form method="post" action="<?php echo $this->config->item('base_url') ."article/update/" .$article['article_id'].".html"?>">
		        <h1 id='article-title'>			   
				<textarea id='edit_title' name='new_title'><?php echo $article['title']?></textarea>
			   </h1>
		       	<p style="text-align: justify">
					<main id='article-html'>
						<textarea name='new_body' id="edit_ta" style="width: 100%;">
							<?php echo $article['body']?>
						</textarea>
					</main>
				</p> 
				<div id="end-div" class="clearfix"></div>
				<hr>
				<div class="clearfix"></div>
				<input id='unique_id' type='hidden' name='unique_id'>
				<p class='article_text'>
					<input id='notes_input' type='text' name='notes' placeholder='الملاحظات، بيكون رائع إذا وضحت سبب التعديل' required>
				<div class='boxes'>
					<input type='hidden' id='unique_id' name='uid'>
					<input type='hidden' value='<?php echo $article['article_id']?>' name='unique_a_id'>

					<p><input type='radio' value='Spelling' id='Spelling' name='Reason' checked>
					<label for='Spelling'>Spelling</label></p>	

					<p><input type='radio' value='Grammar' id='Grammar' name='Reason'>
					<label for='Grammar'>Grammar</label></p>

					<p><input type='radio' value='Scientific' id='Scientific' name='Reason'>
					<label for='Scientific'>Scientific</label></p>

					<p><input type='checkbox' id='crucial' name='crucial'>
					<label for='crucial'>Crucial</label></p>
					<p class='article_text'>
						<small>قبل حفظ التغييرات، تأكد من تقديمها كلها في وقت واحد، إذا لا يمكن تطبيق أكثر من تعديل، حيث يؤخذ التعديل الأخير فقط بعين الإعتبار</small>
					</p>
					<input type='submit' class='article_text' value='حفظ التغييرات'></input>
					
					<input type='button' class='article_text' value='إلغاء' onclick="window.location = home_url + 'article/<?php echo $article['article_id']?>.html'"></input>
				</div>
			</div>
			</form>
	</article>