<div class="container user-profile">
	<div class="col-md-6 col-md-push-6">
		<div>
			 <div class="well profile">
				  <div class="col-sm-12">
					 <div class="col-xs-12 col-sm-8">
						<h2><?php echo $user['name']?></h2>
						<p><strong>نبذة: </strong> <?php echo $user['desc']?> </p>
						<p><strong>الأقسام: </strong><a style='display:inline; color: #333' class="hint--bottom" data-hint="الأقسام التي عمل بها الشخص أكثر من ٥ مقالات"><?php echo implode('، ', $user['cats']) ?></a></p>
					 </div>             
					 <div class="col-xs-12 col-sm-4 text-center">
						<figure>
						    <img src="http://www.syr-res.com/team_pics/get.php?id=<?php echo $user['fb_id']?>" alt="" class="img-circle img-responsive">
						</figure>
					 </div>
				  </div>            
				  <div class="col-xs-12 divider text-center">
					 <div class="col-xs-12 col-sm-4 emphasis">
						<h2><strong> <?php echo count($user['arts'])?> </strong></h2>                    
						<p><small>عمل</small></p>
						<a href="mailto:<?php echo $user['email']?>"><button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span><span class="genericon genericon-mail"></span></button></a>
					 </div>
					 <div class="col-xs-12 col-sm-4 emphasis">
						<h2><strong><?php echo $user['uviews']?> </strong></h2>                    
						<p><small>مشاهدة الصفحة الشخصية</small></p>
						<a target="_blank" href="<?php echo $user['twitter']?>"><button class="btn btn-info btn-block"><span class="fa fa-user"></span><span class="genericon genericon-twitter"></span></button></a>
					 </div>
					 <div class="col-xs-12 col-sm-4 emphasis">
						<h2><strong><?php echo count($user['cats'])?> </strong></h2>                    
						<p><small>قسم</small></p>
						<a target="_blank" href="https://www.facebook.com/<?php echo $user['fb_id']?>"><button class="btn btn-fb btn-block"><span class="fa fa-user"></span><span class="genericon genericon-facebook"></span></button></a>
					 </div>
				  </div>
			 </div>                 
		</div>
	</div>
	<div class="col-md-6 well col-md-pull-6">
		<h3>إنجازات</h3>
		<ul>
			<?php
				foreach($user['arts'] as $a)
				{
					echo "<li><a data-asset='{$this->config->item('base_url')}article/{$a->article_id}.json' href='{$this->config->item('base_url')}article/{$a->article_id}.html'>".$a->title."</a></li>";
				}
			?>
		</ul>
	</div>
	
</div>
