	<section>
		<div class="container">
			<div class="col-md-6">
				<div id='stretcher'>
					<div class="most-read-title">
						<h3>آخر المواضيع</h3>
					</div>

				<?php
				$i = 0;
				foreach($recent_arts as $a)
				{
				if($i++ > 9) break;
				?>					
					<div class="element" style="">
						<div class='element-image-holder' style="background-image: url('http://www.syr-res.com/pic_ret.php?id=<?php echo $a->home_pic_id?>')">
						<div class='element-stats-strip'><span class="stats-genericon genericon-comment"></span><p><?php echo $a->fb_share_count?></p><span class="stats-genericon genericon-show"></span><p><?php echo $a->views ?></p></div>
							<a data-asset="<?php echo $this->config->item('base_url') . "article/".$a->article_id . ".json"?>" href="<?php echo $this->config->item('base_url') ."article/".$a->article_id . ".html"?>">
								<div class="element-text-shade">					
									<div class='element-row'>							
										<div class='element-cell'>											
											<h3><?php echo $a->title?></h3>
										</div>		
									</div>
								</div>	
							</a>
						</div>
					</div>		
				<?php
				}
				?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="most-read">
					<div class="most-read-title">
						<h3>أكثر خمس مواضيع قراءة</h3>
					</div>
					<div class="most-read-body">
						<div id="slider">
							<div id="mask">  <!-- Mask -->
								<ul>
									<li id="first" class="firstanimation" style="display: table-cell; vertical-align: middle; text-align: center">
										<a data-asset="<?php echo $this->config->item('base_url') . "article/".$top_read[0]->article_id . ".json"?>" href="<?php echo $this->config->item('base_url') ."article/".$top_read[0]->article_id . ".html"?>">
											<div class='most-read-table'>
												<div  class='most-read-tr'>
													<div class='most-read-td'>
														<h3><?php echo $top_read[0]->title?></h3>
														<h4><?php echo $top_read[0]->subcatname?></h4>
													</div>
												</div>
											</div>
										</a>
									</li>	
									<li id="second" class="secondanimation" style="display: table-cell; vertical-align: middle; text-align: center">
										<a data-asset="<?php echo $this->config->item('base_url') . "article/".$top_read[1]->article_id . ".json"?>" href="<?php echo $this->config->item('base_url') ."article/".$top_read[1]->article_id . ".html"?>">
											<div class='most-read-table'>
												<div  class='most-read-tr'>
													<div class='most-read-td'>
														<h3><?php echo $top_read[1]->title?></h3>
														<h4><?php echo $top_read[1]->subcatname?></h4>
													</div>
												</div>
											</div>
										</a>
									</li>	
									<li id="third" class="thirdanimation" style="display: table-cell; vertical-align: middle; text-align: center">
										<a data-asset="<?php echo $this->config->item('base_url') . "article/".$top_read[2]->article_id . ".json"?>" href="<?php echo $this->config->item('base_url') ."article/".$top_read[2]->article_id . ".html"?>">
											<div class='most-read-table'>
												<div  class='most-read-tr'>
													<div class='most-read-td'>
														<h3><?php echo $top_read[2]->title?></h3>
														<h4><?php echo $top_read[2]->subcatname?></h4>
													</div>
												</div>
											</div>
										</a>
									</li>	
									<li id="fourth" class="fourthanimation" style="display: table-cell; vertical-align: middle; text-align: center">
										<a data-asset="<?php echo $this->config->item('base_url') . "article/".$top_read[3]->article_id . ".json"?>" href="<?php echo $this->config->item('base_url') ."article/".$top_read[3]->article_id . ".html"?>">
											<div class='most-read-table'>
												<div  class='most-read-tr'>
													<div class='most-read-td'>
														<h3><?php echo $top_read[3]->title?></h3>
														<h4><?php echo $top_read[3]->subcatname?></h4>
													</div>
												</div>
											</div>
										</a>
									</li>	
									<li id="fifth" class="fifthtanimation" style="display: table-cell; vertical-align: middle; text-align: center">
										<a data-asset="<?php echo $this->config->item('base_url') . "article/".$top_read[4]->article_id . ".json"?>" href="<?php echo $this->config->item('base_url') ."article/".$top_read[4]->article_id . ".html"?>">
											<div class='most-read-table'>
												<div  class='most-read-tr'>
													<div class='most-read-td'>
														<h3><?php echo $top_read[4]->title?></h3>
														<h4><?php echo $top_read[4]->subcatname?></h4>
													</div>
												</div>
											</div>
										</a>
									</li>	
									
								</ul>
							</div>
						</div>
					</div>					
				</div>
				<div class="most-read">
					<div class="most-read-title">
						<h3>أخبار عن المبادرة</h3>
					</div>
					<div class='news-body'>
						<ul>
							<?php foreach($news as $n)
							{
								echo "<li><a href='{$this->config->item('base_url')}article/{$n->article_id}.html' data-asset='{$this->config->item('base_url')}article/{$n->article_id}.json'>{$n->title}</a></li>";
							}
							?>
						</ul>					
					</div>
				</div>
			</div>
		</div>	
	</section>