<body>
	<div class="container">
		<article>
		        <h1><?php echo FixEnglishAndNumbers($article['title'])?></h1>
		        <h3 class="cat-name"><?php echo $article['catname'] . "<span style='color:#444'> >>>> </span>" . $article['catname'] ?></h3>
		        	<p style="text-align: justify">
		        		<img class="article-img" src="http://www.syr-res.com/pictures/57635650.jpg"/>
		        		<?php echo DashesToHR($article['body'])?>    		        				        		
				</p> 
				<hr>
				<div class="share-buttons-div">
					<div class="share-buttons-title">						
						<h3><span class="glyphicon glyphicon-share"></span> شارك </h3>
					</div><div class="share-buttons share-buttons-gplus">						
						<span class="share-genericon genericon-googleplus"></span>
					</div><div class="share-buttons share-buttons-fb">						
						<span class="share-genericon genericon-facebook"></span>
					</div><div class="share-buttons share-buttons-pin">						
						<span class="share-genericon genericon-pinterest"></span>
					</div><div class="share-buttons share-buttons-pocket">						
						<span class="share-genericon genericon-pocket"></span>
					</div><div class="share-buttons share-buttons-twitter">						
						<span class="share-genericon genericon-twitter"></span>
					</div>
				</div>
				<div class="share-buttons-div">
					<div class="share-buttons-title">						
						<h3><span class="glyphicon glyphicon-th-list"></span> تفاصيل</h3>
					</div><div class="details-buttons details-buttons-date">						
						<span class="glyphicon glyphicon-calendar"></span> 
						<?php echo $article['publish_date'] ?>
					</div><div class="details-buttons details-buttons-views">						
						<span class="glyphicon glyphicon-eye-open"></span> 
						<?php echo $article['views'] ?>
					</div><div class="share-buttons details-buttons-original-post">						
						البوست
					</div>
				</div>
				<div class="signatures-buttons-div">
					<div class="signatures-buttons-title">						
						<h3><span class="glyphicon glyphicon-th-list"></span> المساهمون في الإعداد</h3>
					</div>					
					<?php
						foreach($signatures as $s)
						{
							echo '<div class="details-buttons details-buttons-signature">'.$s->label.': '. $s->name.'</div>';
						}
					?>
					
				</div>
				<div style="height: 500px"></div>
		</article>