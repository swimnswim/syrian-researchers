<?php
function AddUser($name, $gender, $password, $level, $desc, $fb_id, $ads_admin)
{
	if(!is_numeric($level)) { echo "Error: Bad level, level must be(1-3)."; return 0;}
	if(strlen($name) < 2) { echo "Error: Bad name (2) chars min."; return 0;}
	if(strlen($password) < 6) { echo "Error: Bad password length must be > 6."; return 0;}
	if(strlen($gender) < 4 || strlen($gender) > 6) { echo "Error: Bad gender."; return 0;}
	if(!is_numeric($ads_admin)) { echo "Error: Bad ads_admin."; return 0;}
	if(strlen($fb_id) < 2) { echo "Error: Bad FB id."; return 0;}
	if(GetUserByFBId($fb_id)) { echo "Error: User already exists."; return 0;}
	
	$password = md5($password);
	$res = $GLOBALS['db']->query("insert into user(`name`, `gender`, `password`, `level`, `desc`, `fb_id`, `ads_admin`) VALUES ('$name', '$gender', '$password', '$level', '$desc', '$fb_id', '$ads_admin')");
	if(!$res) { echo "Error adding user :("; return 0;}
	return $GLOBALS['db']->insert_id;
}
function incrementUserViews($id)
{
	$GLOBALS['db']->query("update user set views=views+1 where id = '$id'");
}
function DeleteUser($id)
{
	if(!is_numeric($id)) { echo "Bad id..."; return 0;}
	$res = $GLOBALS['db']->query("delete from user where id = '$id'");
	if(!$res) { echo "Error: couldn't delete user."; return 0;}
	return 1;
}
function UpdateUser($id, $field, $value)
{
	if(!is_numeric($id)) { echo "Bad id..."; return 0;}
	if(strlen($field) < 2) { echo "Bad field name"; return 0;}
	$res = $GLOBALS['db']->query("update user set `$field` = '$value' where id = '$id'");
	if(!$res) {"Error updateing user.."; return 0;}
	return 1;
}
function GetUserById($id)
{
	if(!is_numeric($id)) { return 0;}
	$res = $GLOBALS['db']->query("select * from user where id = '$id'");
	if($res->num_rows == 0) {return 0;}
	$array = $res->fetch_assoc();
	if($array['super_admin']) $array['level'] = 0;
	return $array;
}
function GetTotalUserViews()
{
	$res = $GLOBALS['db']->query("select sum(views) as s from user");
	if($res->num_rows == 0) {return 0;}
	$array = $res->fetch_assoc();
	return $array;
}
function MaxUserViews()
{
	$res = $GLOBALS['db']->query("select * from user order by views DESC limit 1");
	if($res->num_rows == 0) {return 0;}
	$array = $res->fetch_assoc();
	return $array;
}
function MinUserViews()
{
	$res = $GLOBALS['db']->query("select * from user order by views ASC limit 1");
	if($res->num_rows == 0) {return 0;}
	$array = $res->fetch_assoc();
	return $array;
}
function GetUserByFBId($id)
{
	if(!is_numeric($id)) {return 0;}
	$res = $GLOBALS['db']->query("select * from user where fb_id = '$id'");
	if($res->num_rows == 0) {return 0;}
	$array = $res->fetch_assoc();
	if($array['super_admin']) $array['level'] = 0;
	return $array;
}
function GetUserByArticleId($article_id)
{
	if(!is_numeric($article_id)) { echo "Bad article id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT user_id FROM article where id = '$article_id'");
	if($res->num_rows == 0) {echo "No articles..."; return 0;}
	$id = $res->fetch_assoc();
	$id = $id[0];
	$res = $GLOBALS['db']->query("SELECT * FROM user where id = '$id'");
	if($res->num_rows == 0) {return 0;}
	$result = $res->fetch_assoc();
	if($array['super_admin']) $array['level'] = 0;
	return $result;
}
function GetAllUsers()
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM user order by BINARY(name) ASC");
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		if($row['super_admin']) $row['level'] = 0;
	   $result[$i++] = $row;
	}
	return $result;
}