<?php
include("cnf.php");
function AddSignature($label, $user_id, $article_id, $count = 1)
{
	if(strlen($label) < 3) {return 0;}
	if(!is_numeric($article_id)) { echo "Bad article id..."; return 0;}	
	if(!is_numeric($user_id)) { echo "Bad user id..."; return 0;}	
	if(!is_numeric($count)) { echo "Bad count.."; return 0;}	
	
	$name = addslashes($label);
	$date = date("Y-m-d H:i:s");
	$res = $GLOBALS['db']->query("insert into signature(`label`, `user_id`, `article_id`, `date`, `cnt`) values('$label', '$user_id', '$article_id', '$date', '$count')");
	if(!$res) { return 0; }	
	return $GLOBALS['db']->insert_id;
}
function GetSignaturesByArticleId($article_id)
{
	$result = Array();
	if(!is_numeric($article_id)) { echo "Bad article id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM signature INNER JOIN user ON signature.user_id = user.id where article_id = '$article_id' order by signature.id ASC");
	$i = 0;
	while($array = $res->fetch_assoc())
	{
	   $result[$i++] = $array;
	}
	return $result;
}
function GetUsersByArticleId($article_id)
{
	$result = Array();
	if(!is_numeric($article_id)) { echo "Bad article id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM signature INNER JOIN user ON signature.user_id = user.id where article_id = '$article_id' group by user.id order by signature.id ASC");
	$i = 0;
	while($array = $res->fetch_assoc())
	{
	   $result[$i++] = $array;
	}
	return $result;
}
function GetNamesByArticleId($article_id)
{
	$result = Array();
	if(!is_numeric($article_id)) { echo "Bad article id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM signature INNER JOIN user ON signature.user_id = user.id where article_id = '$article_id' group by user.id order by signature.id ASC");
	$i = 0;
	while($array = $res->fetch_assoc())
	{
	   $result[$i++] = $array;
	}
	return $result;
}
function GetSignaturesByCatIdForStats($cat_id, $start_date, $end_date, $all)
{
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad cat id..."; return 0;}	
	$res = null;
	if($all)
	{
		$res = $GLOBALS['db']->query("Select sum(score) as score, user_id, name from(SELECT user.name, score*(signature.cnt*count(CONCAT(signature.label, signature.user_id))) as score, signature.label, signature.user_id FROM labels_and_score, signature, user, article where labels_and_score.label = signature.label and signature.user_id = user.id and article.deleted = 0 and article.id = signature.article_id and article.cat_id = '$cat_id' group by CONCAT(signature.label, signature.user_id) order by count(CONCAT(signature.label, signature.user_id)) DESC) as child group by(user_id) order by sum(score) DESC");
		while($arr = $res->fetch_assoc())
		{			
			$result[$arr['user_id']] = $arr;
		}
	}
	else
	{
		$res = $GLOBALS['db']->query("Select sum(score) as score, user_id, name from(SELECT user.name, score*(signature.cnt*count(CONCAT(signature.label, signature.user_id))) as score, signature.label, signature.user_id FROM labels_and_score, signature, user, article where labels_and_score.label = signature.label and DATE(`article`.`date`) >= DATE('$start_date') and DATE(`article`.`date`) <= DATE('$end_date') and signature.user_id = user.id and article.deleted = 0 and article.id = signature.article_id and article.cat_id = '$cat_id' group by CONCAT(signature.label, signature.user_id) order by count(CONCAT(signature.label, signature.user_id)) DESC) as child group by(user_id) order by sum(score) DESC");
		$i = 0;
		while($arr = $res->fetch_assoc())
		{
			$result[$i++] = $arr;
		}
	}

	return $result;
}
function LazyUsers($peek, $cat_id)
{
	$result = Array();
	$peek = $GLOBALS['db']->escape_string($peek);

	$res = $GLOBALS['db']->query("SELECT max(signature.date) as md, user.fb_id, user.name, signature.user_id, count(signature.user_id) as cnt FROM signature, article, user where signature.user_id = user.id and article.cat_id = '$cat_id' and article.id = signature.article_id group by signature.user_id having MAX(DATE(`signature`.`date`)) < DATE('$peek') order by count(*) DESC");
	$i = 0;
	while($arr = $res->fetch_assoc())
	{
		$score = GetUserScoreFull($arr['user_id']);
	   $arr['total_score'] = $score[1];
	   $result[$i++] = $arr;	   
	}
	return $result;
}
function NeverWorkedUsers()
{
	$result = Array();
	$res = $GLOBALS['db']->query("SELECT name,fb_id,id from user where id NOT IN (select user_id from signature)");
	$i = 0;
	while($arr = $res->fetch_assoc())
	{
	   $result[$i++] = $arr;
	}
	return $result;
}
function GetAllLabels()
{
	$result = array();
	$res = $GLOBALS['db']->query("SELECT label FROM signature group by label ORDER BY COUNT( label ) DESC ");

	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetSignaturesByUserId($user_id)
{
	$result = Array();
	if(!is_numeric($user_id)) { echo "Bad user id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM signature where user_id = '$user_id' order by binary label");
	$i = 0;
	while($arr = $res->fetch_assoc())
	{
	   $result[$i++] = $arr;
	}
	return $result;
}
function GetLabelsByUserId($user_id)
{
	$result = Array();
	if(!is_numeric($user_id)) { echo "Bad user id..."; return 0;}	
	$res = $GLOBALS['db']->query("SELECT count(label) as c, label, `cnt` FROM signature where user_id = '$user_id' and date >= '".date("Y-m-d H:i:s", time() - 2592000)."' group by label");
	$i = 0;
	while($arr = $res->fetch_assoc())
	{
	   if($arr['label'] == 'تعديل') continue;
	   $result[$i++] = $arr;
	}
	return $result;
}
function GetLabelsByUserIdFull($user_id)
{
	$result = Array();
	if(!is_numeric($user_id)) { echo "Bad user id..."; return 0;}	
	$res = $GLOBALS['db']->query("SELECT count(label) as c, label, `cnt` FROM signature where user_id = '$user_id' group by label");
	$i = 0;
	while($arr = $res->fetch_assoc())
	{
	   if($arr['label'] == 'تعديل') continue;
	   $result[$i++] = $arr;
	}
	return $result;
}
function cmp($a, $b)
{
    if($a[1] == $b[1]) return 0;
	if($a[1] < $b[1]) return 1;
	if($a[1] > $b[1]) return -1;
}
function GetAllScores()
{
	$users = GetAllUsers();
	$result = array();
	$i = 0;
	foreach($users as $u)
	{
		$result[$i++] = GetUserScore($u[id]);	
	}	
	uasort($result, "cmp");
	//$result = array_slice ($result, 0, 30);
	return $result;
}
function GetAllScoresFullTime()
{
	$users = GetAllUsers();
	$result = array();
	$i = 0;
	foreach($users as $u)
	{
		$result[$i++] = GetUserScoreFull($u[id]);	
	}	
	uasort($result, "cmp");
	//$result = array_slice ($result, 0, 30);
	return $result;
}
function LabelToNumber($label)
{
	$res = $GLOBALS['db']->query("select score from labels_and_score where label = '$label'");
	if(!$res) { return 1; }	
	$score = $res->fetch_assoc();
	$score = $score['score'];
	return $score;
}
function GetUserScore($id)
{
	$labels = GetLabelsByUserId($id);
	$finalScore = 0;
	foreach($labels as $l)
	{
		$c = $l[c];
		$count = $l['cnt'];
		$l = trim($l[label]);
		$score = 0;		
		$label_score = LabelToNumber($l);
		$score = $label_score * $c * $count;		
		$finalScore += $score;	
	}
	return array($id, $finalScore, $labels);
}
function GetUserScoreFull($id)
{
	$labels = GetLabelsByUserIdFull($id);
	$finalScore = 0;
	foreach($labels as $l)
	{
		$c = $l[c];
		$count = $l['cnt'];
		$l = trim($l[label]);
		$score = 0;		
		$label_score = LabelToNumber($l);
		$score = $label_score * $c * $count;		
		
		$finalScore += $score;	
	}
	return array($id, $finalScore, $labels);
}
function DeleteSignatureByArticleId($article_id)
{
	$res = $GLOBALS['db']->query("delete FROM signature where article_id = '$article_id'");
}
function GetSignatureByLabel($label, $article_id)
{
	if(!is_numeric($article_id)) { return 0;}	
	$res = $GLOBALS['db']->query("select * from signature where article_id = '$article_id' and label = '$label'");
	if(!$res) { return 0; }	
	return $res->fetch_assoc();
}
function DisableSignature($article_id)
{
	if(!is_numeric($article_id)) { return 0;}	
	$GLOBALS['db']->query("update signature set `cnt` = 0 where article_id = '$article_id'");
}
function UpdateSignature($id, $f, $v)
{
	if(!is_numeric($id)) { return 0;}	
	$date = date("Y-m-d H:i:s");
	$GLOBALS['db']->query("update signature set `$f` = '$v' where id = '$id'");
	$GLOBALS['db']->query("update signature set `date` = '$date' where id = '$id'");
}
?>