<?php
require_once("db_updates.php");
function AddCat($name)
{
	// 1 ok
	// -2 already exists
	// -3 bad name
	if(strlen($name) < 2) {echo "Bad name, (2) chars min."; return -3;}
	
	$name = addslashes($name);
	if(GetCatByName($name)) { "Error inserting category, already exists.."; return -2; }	
	
	$res = $GLOBALS['db']->query("insert into category(`name`) values('$name')");
	if(!$res) {echo "Error inserting category.."; return 0;}	
	return $GLOBALS['db']->insert_id;
}

function UpdateCatName($id, $newName)
{
	if(strlen($newName) < 2) {echo "Bad name, (2) chars min."; return 0;}
	if(!is_numeric($id)) {echo "Bad cat id.."; return 0;}
	
	$newName = addslashes($newName);
	
	if(GetCatByName($newName)) {return 0;}
	
	$res = $GLOBALS['db']->query("update category set `name`='$newName' where id = '$id'");
	if(!res) {echo "Error updating cat..."; return 0;}
	AddUpdate("cat", "update", $id);
	return 1;
}
function UpdateCatOrder($id, $ord)
{
	if(!is_numeric($id)) {echo "Bad cat id.."; return 0;}
	if(!is_numeric($ord)) {echo "Bad cat ord.."; return 0;}
	
	$res = $GLOBALS['db']->query("update category set `ord`='$ord' where id = '$id'");
	if(!res) {echo "Error updating cat..."; return 0;}
	return 1;
}
function AddAdminsToCat($u1, $u2, $u3, $u4, $cat_id)
{
	if(!is_numeric($u1)) {return -1;}
	if(!is_numeric($cat_id)) {return -2;}
	
	$res = $GLOBALS['db']->query("insert into cat_and_admin(`u1`,`u2`,`u3`,`u4`, `cat_id`) values('$u1','$u2','$u3','$u4', '$cat_id')");
	if(!$res)
	{
		return 0;
	}
	return 1;	
}
function DeleteAdmins($cat_id)
{
	if(!is_numeric($cat_id)) {return 0;}	
	$res = $GLOBALS['db']->query("delete from cat_and_admin where `cat_id` = '$cat_id'");
	if(!$res)
		return 0;
	return 1;	
}
function GetCatAdmins($cat_id)
{
	if(!is_numeric($cat_id)) {return 0;}	
	$result = Array();

	$res = $GLOBALS['db']->query("select * from `cat_and_admin` where `cat_id` = '$cat_id'");
	$row = $res->fetch_assoc();
	$i = 0;
	$result[$i++] = GetUserById($row['u1']);	
	$result[$i++] = GetUserById($row['u2']);	
	$result[$i++] = GetUserById($row['u3']);	
	$result[$i++] = GetUserById($row['u4']);		
		
	return $result;
}
function DeleteCat($id)
{
	
	if(!is_numeric($id)) {echo "Bad cat id.."; return 0;}
	$res = $GLOBALS['db']->query("delete from category where id = '$id'");
	if(!$res) {echo "Error deleting cat..."; return 0;}
	$res = $GLOBALS['db']->query("delete from sub_cat where mother_cat = '$id'");
	if(!$res) {echo "Error deleting its sub cats..."; return 0;}
	AddUpdate("cat", "delete", $id);
	return 1;
}
function GetCat($id)
{
	if(!is_numeric($id)) { return 0;}
	$res = $GLOBALS['db']->query("select * from category where id = '$id'");
	if(!$res) return 0;
	$row = $res->fetch_assoc();
	if(!$row) {return 0;}
	return $row;
}
function GetAllCats()
{
	$result = Array();
	$res = $GLOBALS['db']->query("select * from category order by ord");
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		$result[$i] = $row;
		$i++;
	}	
	if($i == 0) return 0;
	return $result;
}
function GetAllCatsByUser($user_id)
{
	$res = $GLOBALS['db']->query("SELECT * FROM category INNER JOIN user_and_cats ON user_and_cats.cat_id = category.id where user_id = '$user_id' group by cat_id");
	if(!$res) return 0;
	$arr = Array();
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		$arr[$i++] = $row;
	}
	return $arr;
}


function GetAllParentCats() // cats who have children
{
	$result = Array();

	$res = $GLOBALS['db']->query("select * from category");
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		if(GetAllsubCatsByMotherId($row['id']))		
			{$result[$i] = $row; 	$i++;}
	
	}	
	return $result;
}
function GetCatByName($name)
{
	$name = addslashes($name);
	$res = $GLOBALS['db']->query("select * from category where name = '$name'");
	$row = $res->fetch_assoc();
	if(!$row) {return 0;}
	return $row;
}
function GetCatByArticleId($article_id)
{
	if(!is_numeric($article_id)) { echo "Bad article id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT cat_id FROM article where id = '$article_id'");
	$id = $res->fetch_assoc($res);
	$id = $id[0];

	$res = $GLOBALS['db']->query("SELECT * from category where id = '$id'");
	$result = $res->fetch_assoc();
	return $result;	
}

?>