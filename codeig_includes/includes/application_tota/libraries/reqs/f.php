توم وجيري مسلسل أغرق طفولتنا ضحكات ... وموسيقى<br><br>
من منا يستطيع تخيل توم وجيري بدون موسيقى؟ لقد تم تأليف موسيقى مذهلة لهذا المسلسل أروعها كانت للموسيقي سكوت برادلي، يمكنك قراءة <a href="http://www.syr-res.com/article.php?id=3003" target="_blank">مقالنا عنه</a>.<br><br>
ولكن كثيراً ما نسمع موسيقى كلاسيكية فنقول "أين سمعناها من قبل؟ نعم ! إنها من توم وجيري"<br><br>
لنتعرف سوية على أشهر المقطوعات التي تم توظيفها واستخدامها في موسيقى توم وجيري في مقال من جزأين يتناول المقطوعات حسب تسلسل الحلقات التي سنعرضها كاملة مع ذكر التوقيت الذي تظهر فيه تلك المقطوعات.<br><br>
1- نبدأ بأغنية Mamae eu quero" " من الحلقة رقم 12 بعنوان Baby puss وهي ل jararaca paiva and vicente paiva<br><br>
أغنية "أريد أمي " هي أغنية برزت في القرن العشرين "في الأربعينيات من القرن" غنتها فتاة تدعى كارمن ميراندا "المرأة البرازيلية التي تلبس فواكه استوائية كقبعة لرأسها" .. لا أحد من الناس وبالأخص شعب البرازيل لا يعرف بهذه الأغنية الرائعة والتي تستخدم كثيرا في الاحتفالات في البرازيل<br><br>
الأغنية الأصلية<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/Ee0Pw5pDiEM?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
الأغنية كما سمعناها في توم وجيري في الدقيقة 6:02<br><br>
<iframe src="//player.vimeo.com/video/104646445" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> 
<br />
<a href="http://vimeo.com/104646445">Tom and Jerry [012] Baby Puss</a> from <a href="http://vimeo.com/user31677394">Sulieman</a> on <a href="https://vimeo.com">Vimeo</a>.<br><br>
2- في الحلقة 26 بعنوان Solid serenade نسمع أغنية Is You Is or Is You Ain't My Baby<br><br>
أغنية "هل أنت هو أو هو أنت حبيبي " هي للمؤلف لويس جوردان "عازف البلوز المشهور في أواخر الأربعينيات في أيام الحرب العالمية الثانية" .. كان لويس رجل استعراض وعازف ساكسوفون ومؤلف أغاني عديدة .. وتعد جملة " هل أنت هو أو هو أنت حبيبي" لهجة ظهرت اول مرة في عام 1921 في رواية اوكتافوس للكاتب روي كوهين من كارولينا الجنوبية <br><br>
الأغنية الأصلية<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/dt6-ArY-LcY?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
كما سمعناها في توم وجيري في الدقيقة 1:16<br><br>
<iframe src="//player.vimeo.com/video/104646447" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <p><a href="http://vimeo.com/104646447">Tom and Jerry [026] Solid Serenade</a> from <a href="http://vimeo.com/user31677394">Sulieman</a> on <a href="https://vimeo.com">Vimeo</a>.<br><br>
ويعاد تكرار نفس المشهد في الحلقة رقم 66 بعنوان Smitten Kitten<br><br>
3- نأتي إلى يوم حفل توم الموسيقي الرائع ومن منا لا يتذكر هذه الحلقة الرائعة رقم 29 بعنوان the cat concerto والتي عزفت فيها مقطوعة hungarian rhapsody no.2" for Franz liszt"<br><br>
كتبت هذه القطوعة على نسختين لتعزف على البيانو أو عبر الأوركسترا كاملة وتعد هذه الرابسودي الهنغارية الثانية من مجموعة 19 رابسودي ألفها فرانز ليست، وتم بناؤها على أغان هنغارية شعبية عدل سكوت برادلي بعبقرية الموسيقى لتناسب المقاطع و الحركات التي تجري على الشاشة (مثل خروج جيري من البيانو و المشاجرات بينهما، أو في المقطع الذي أصبحا يطاردان بعضهما و يعزفان على البيانو بنفس الوقت)، في هذه المقاطع انتقلت الموسيقى بسلاسة من الكلاسيك إلى الجاز باسلوب عبقري ثم الكلاسيك مرة أخرى، و عدل عليها الكثير من المقاطع بشكل لافت دون أي إنقاص في قيمة ووزن الموسيقى الأصلية.<br><br>
البيانو <br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/LdH1hSWGFGU?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
الاوركسترا<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/pk4Gu3a89Rk?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
توم وجيري <br><br>
<iframe src="//player.vimeo.com/video/104646455" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <p><a href="http://vimeo.com/104646455">Tom and Jerry [029] The Cat Concerto DvDRip Mp3 XvID [1947]</a> from <a href="http://vimeo.com/user31677394">Sulieman</a> on <a href="https://vimeo.com">Vimeo</a>.<br><br>
ونلاحظ أننا في شارة البداية نسمع لحن البريليود رقم 24 في سلم ري مينور لشوبان<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/-FoABv3IhDg?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
4- في الحلقة رقم 34 بعنوان kitty foiled نسمع لحن The Barber of Seville<br><br>
العمل للمؤلف الايطالي روسيني الموسيقى التي استخدمت في توم وجيري هي من مقدمة الأوبرا وتعتبر هذه الأوبرا من أعظم الأوبرات الموسيقية التي تحتوي على الكوميديا .. وقد تم تسميتها الاوبرا الأكثر فكاهية بين الجميع.<br><br>
المقدمة من الأوبرا الأصلية<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/8mVfVaqGZnQ?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
كما سمعناها في توم وجيري في الدقيقة 6:09<br><br>
<iframe src="//player.vimeo.com/video/104184792" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <br><br>
5- في الحلقة 52 بعنوان The Hollywood Bowl نسمع لحن Die Fledermaus <br><br>
تعتبر حكاية "الخفافيش" او ال"دي فليديرماوس" حكاية نموذجية نجد فيها الكثير من الحب .. وقد كتبت هذه الموسيقى لهذا الغرض عن طريق يوهان شتراوس الثاني<br><br>
مقدمة العمل الأصلي<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/sHF5LP53LZY?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
كما سمعناها في توم وجيري<br><br>
<iframe src="//player.vimeo.com/video/104185293" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <br><br>
6- في الحلقة 63 بعنوان The flying cat نسمع لحن Grande valse brillante in E-flat major <br><br>
هذه الموسيقى الرائعة للأسطورة "شوبان" تم نشرها عام 1834 .. وقد كانت هذه أولى منشورات مؤلفات الفالس للبيانو المنفرد على الرغم أنه قبل عام 1834 ألف شوبان على الاقل 16 فالس وتم تدميرها أو نشرها بعد وفاته .. وكان شوبان قد أعطى لقب "الفالس العظيم الرائع" لهذه المعزوفة والفالسات الثلاثة التي تلتها في عام 1838<br><br>
الفالس الأصلي<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/LG-E4PVGQSI?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
كما سمعناه في توم وجيري في الدقيقة 3:16<br><br>
<iframe src="//player.vimeo.com/video/104185086" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <br><br>
7- الحلقة 65 في توم وجيري بعنوان The Two Mouseketeers استخدمت أغنية القبرة أو "Alouette" وهي أغنية شعبية فرنسية-كندية للأطفال تحكي عن عقاب قبرة عن طريق نزع ريشها وذلك لأن غناءها أيقظ النائم.أصل الأغنية مجهول وقد صدرت أول مرة في كتيب للتلاميذ والخريجين في احدى جامعات مونتريال عام 1879 . تعلمها العديد من جنود المشاة الأمريكيين وجنود حلفاء أخرون أثناء وجودهم في فرنسا في الحرب العالمية الأولى وجلبوها الى بلادهم لتصل الى أبنائهم وأبناء أبنائهم.<br><br>
الأغنية الأصلية:<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/xM0UyNqrS0o?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
كما سمعناها في توم وجيري في الدقيقة 3:40<br><br>
<iframe src="//player.vimeo.com/video/104185369" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <br><br>
8- في الحلقة 75 بعنوان Johann Mouse نسمع مجموعة من ألحان يوهان شتراوس وهي <br><br>
Emperor Waltz<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/LAVvBF7m260?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
The Blue Danube Waltz (بإمكانكم التعرف على رقص الفالس وتاريخها وروادها وأعلامها من<a style="font-weight: bold; color: #23B0FC" target="_blank" href=" http://www.syr-res.com/TV/3072.html">هنا</a> )<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/ZpQ-wjS0PbU?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
Tritsch-Tratsch Polka<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/u5yr-r3GkW4?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
Perpetuum Mobile op 257<br><br>
<iframe class="yt_iframe" style="width: 640px; height: 426.666666666667px;" width="720" height="480" src="https://www.youtube.com/embed/b-2j_27FeH0?feature=player_embedded&amp;theme=light" frameborder="0" allowfullscreen=""></iframe><br><br>
حلقة توم وجيري<br><br>
<iframe src="//player.vimeo.com/video/104228236" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> <br><br>
موسيقى توم وجيري ... بوابة طفولتنا على الموسيقى العالمية.<br><br>
تابعوا <a href="http://www.syr-res.com/article.php?id=3114" target="_blank">الجزء الثاني من المقال</a>