<?php
require_once("cnf.php");
require_once("db_updates.php");

function AddVideo($cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $publish_date, $video_id = "")
{
	if(!is_numeric($cat_id)) {return 0;}
	if(!is_numeric($user_id)) {return 0;}
	if(!is_numeric($home_pic_id)) {return 0;}

	$body  = str_replace('\'', '&#39;', $body);
		
	$body = $GLOBALS['db']->escape_string($body);
	$publish_date = $GLOBALS['db']->escape_string($publish_date);
	$video_id = $GLOBALS['db']->escape_string($video_id);

	if(strlen($title) < 3) { return 0; }
	$date = date("Y-m-d H:i:s"); 
	$title = str_replace('\'', '&#39;', $title);

	$body = nl2br($body);

	$q = "insert into video(`cat_id`,  `sub_cat_id`,  `user_id`,  `title`,  `body`,  `home_pic_id`,  `publish_date`,  `video_id`) VALUES('$cat_id',  '$sub_cat_id',  '$user_id',  '$title',  '$body',  '$home_pic_id',  '$publish_date',  '$video_id')";	
	if(!$result = $GLOBALS['db']->query($q))
		return 0;
	return $GLOBALS['db']->insert_id;
}
function incrementViews($id)
{
	if(!is_numeric($id)) { return 0; } 

	$GLOBALS['db']->query("update video set views=views+1 where id = '$id'");
	$result = $GLOBALS['db']->query("select * from video where id = '$id'");
	if(!$result)
	{ return 0;	}
	if($result->num_rows == 0) { return 0;}
	$arr = $result->fetch_assoc();
	return $arr['views'];
}
function UpdateVideo($id, $cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $publish_date)
{
	if(!is_numeric($cat_id)) {return 0;}
	if(!is_numeric($user_id)) {return 0;}
	if(!is_numeric($home_pic_id)) {return 0;}
	if(!is_numeric($id)) {return 0;}

	$body  = str_replace('\'', '&#39;', $body);
		
	$body = $GLOBALS['db']->escape_string($body);
	$publish_date = $GLOBALS['db']->escape_string($publish_date);
	$video_id = $GLOBALS['db']->escape_string($video_id);

	if(strlen($title) < 3) { return 0; }
	$date = date("Y-m-d H:i:s"); 
	$title = str_replace('\'', '&#39;', $title);
	
	$q = "update video set `sub_cat_id`='$sub_cat_id', `cat_id`='$cat_id', `user_id`='$user_id', `title`='$title', `body`='$body', `home_pic_id`='$home_pic_id', `publish_date` = '$publish_date' where id = '$id'";	
	
	if(!$result = $GLOBALS['db']->query($q))
		return -1;
	
	AddUpdate("video", "update", $id);
	return $id;
}
function DeleteVideo($id)
{
	if(!is_numeric($id)) { echo "Error: Bad id..."; return 0;}
	if(!$result = $GLOBALS['db']->query("update video set `deleted` = '1' where id = '$id'"))
		{ echo "Error: an error occured while deleting video..."; return 0;}
	AddUpdate("video", "delete", $id);
	return 1;
}
function GetVideoById($id)
{	
	if(!is_numeric($id)) {  exit; return 0; }
	$result = $GLOBALS['db']->query("select *  from video where id = '$id' and deleted = '0'");
	if(!$result)
	{	echo "No such video..."; return 0;	}
	if($result->num_rows == 0) { return 0;}
	$arr = $result->fetch_assoc();
	$arr['body'] = str_replace("<3", "♥", $arr['body']);
	$arr['title'] = str_replace("<3", "♥", $arr['title']);
	return $arr;
}
function GetAllVideos($page)
{
	if($page == "") $page = 1;
	$page--;
	$start = $page * 20;
	$result = Array();
	
	$res = $GLOBALS['db']->query("SELECT * FROM video where deleted = '0' and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC limit $start, 20");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetNextVideos($id)
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT youtube_id, title, id, home_pic_id FROM video where deleted = '0' and publish_date <= '".date("Y-m-d H:i:s")."' and id < '$id' order by id DESC limit 0, 100");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return str_replace("\n", " ", json_encode($result));
}
function GetHistoryVideosBySubcat($id, $cat_id)
{
	$result = Array();
	$q = "SELECT * FROM video where id < $id and publish_date <= '".date("Y-m-d H:i:s")."' and deleted = 0 and sub_cat_id = $cat_id order by id DESC limit 20";
	$res = $GLOBALS['db']->query($q);
	if($res->num_rows == 0) { return 0; }
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllAllVideos()
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM video where deleted = '0' order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
?>