<?php
require_once("cnf.php");

function UpdateCover($number, $picId, $href, $userId, $enabled)
{
	if(!is_numeric($number)) { return -5;}
	if(!is_numeric($picId)) { return -4;}
	if(!is_numeric($enabled)) { return -3;}
	if(strlen($href) < 5 || strlen($href) > 80) { return -2;}
	
	$href = str_replace('\'', '&#39;', $href);
	
	$res = $GLOBALS['db']->query("update cover set `pic_id`='$picId', `href`='$href', `user_id`='$userId', `enabled` = '$enabled' where number = '$number'");
	if(!$res) {return -1;}
	return 1;//ok
}
function EnableCover($number, $enable)
{
	if(!is_numeric($number)) { return 0;}
	if(!is_numeric($enable)) { return 0;}
	
	$enabled = ($enable > 0) ? "1" : "0";
		
	$res = $GLOBALS['db']->query("update cover set `enabled`='$enabled' where number = '$number'");
	if(!res) {return 0;}
	return 1;
}
function GetAllCovers($getDisabled = 0)
{
	$result = Array();
	if(!is_numeric($getDisabled)) return;
	$res = null;
	
	if(!$getDisabled)
		$res = $GLOBALS['db']->query("SELECT * FROM cover where enabled > 0");
	else
		$res = $GLOBALS['db']->query("SELECT * FROM cover");
		
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}

?>