<?php
require_once("db_article.php");
function AddStat($art, $fb_comments, $fb_shares, $fb_likes, $fb_reach)
{	
	$fbid = $GLOBALS['db']->escape_string($art[fbid]);
	$fb_comments = $GLOBALS['db']->escape_string($fb_comments);		
	$fb_shares = $GLOBALS['db']->escape_string($fb_shares);		
	$fb_likes = $GLOBALS['db']->escape_string($fb_likes);		
	$fb_reach = $GLOBALS['db']->escape_string($fb_reach);		
	
	$exists = StatExists($fbid);	
	$localid = $art[id];
	$owner_id = $art[user_id];
	$local_views = $art['views'];
	$local_shares = $art['fb_share_count'];
	
	if($exists)
	{	
		$q = "update stats set `fb_comments` = GREATEST(`fb_comments`, '$fb_comments'), `fb_shares` = GREATEST(`fb_shares`, '$fb_shares'), `fb_likes` = GREATEST(`fb_likes`, '$fb_likes'), `fb_reach` = GREATEST(`fb_reach`, '$fb_reach'), `local_views` = GREATEST(`local_views`, '$local_views'), `local_shares` = GREATEST(`local_shares`, '$local_shares') where id = $exists";
		if(!$result = $GLOBALS['db']->query($q))
		{
			return -1;
		}
		return 1;
	}
	else
	{		
		$q = "insert into stats(`localid`, `fbid`, `fb_comments`, `fb_shares` ,`fb_likes`,`fb_reach`, `local_views`, `local_shares`) values($localid, '$fbid', '$fb_comments', '$fb_shares' ,'$fb_likes', '$fb_reach',  '$local_views',  '$local_shares')";
		$GLOBALS['db']->query($q);
		return $GLOBALS['db']->insert_id;
	}
}
function StatExists($fbid)
{
	$fbid = $GLOBALS['db']->escape_string($fbid);
	$q = "select * from stats where fbid = '$fbid'";
	$res = $GLOBALS['db']->query($q);
	
	if($res->num_rows == 0)
	{
		return 0;
	}
	else
	{
		$row = $res->fetch_assoc();
		return $row['id'];
	}	
}
function GetStat($id)
{
	$id = $GLOBALS['db']->escape_string($id);
	$q = "select * from stats where localid = '$id'";
	$res = $GLOBALS['db']->query($q);	
	if($res->num_rows == 0)
	{
		return 0;
	}
	else
	{
		$row = $res->fetch_assoc();
		return $row;
	}	
}
function UpdateStat($id, $f, $v)
{
	if(!is_numeric($id)) { return 0;}
	if(!is_numeric($v)) { return 0;}
	$f = $GLOBALS['db']->escape_string($f);
	$f = "`".$f."`";	
	$GLOBALS['db']->query("update stat set $f = '$v' where id = '$id'");
}
?>