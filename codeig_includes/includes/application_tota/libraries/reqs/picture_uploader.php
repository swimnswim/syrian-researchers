<?php
include("cnf.php");
include("db_pic.php");
include("/home/syrresco/public_html/admin_cp_tt/admin_new/login/db.php");

if(!isLoggedIn())
{
	echo "<center>Sorry but you do not have access here, you have to log in first :)</center>";
	return;
}
/*
100: not an image
101: not a jpeg
200: OK
*/
function downloadImage($url, $dest)
{
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_FOLLOWREDIRECT, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$data = curl_exec($ch);
	curl_close($ch);
	
	if(!file_exists($data))
	{
		file_put_contents($dest, $data);
		return true;
	}
	return $false;
}
$images_dir = "/home/syrresco/public_html/pictures/";

$rename = false; //rename file if already exists
$alt = $_POST['alt'];
if($alt == "") $alt = "Syrian Researchers";

$random = substr(md5(rand()),0, rand(5,15));

// strip file_name of slashes

$file_name = GetProperID() . (rand(1,20000));
$new_file_name = $file_name;
$i = 2;
while(file_exists($images_dir . $new_file_name))
{
	$rename = true;
	$new_file_name = $i . '_' . $file_name;
	$i++;
}
if($_FILES['uploadFile']['type'] != "image/jpeg")
{
	echo "<script type=text/javascript>parent.location= \"javascript:result('101');\"</script>";
	return;
}
$copy = move_uploaded_file($_FILES['uploadFile']['tmp_name'],  $images_dir . $new_file_name . ".jpg");

 // check if successfully copied
 if($copy){
	$size = getimagesize( $images_dir . $new_file_name .".jpg"); 
	if(!$size)
	{
		echo "<script type=text/javascript>parent.location= \"javascript:result('100');\"</script>";
		unlink( $images_dir . $new_file_name .".jpg");
		return;	
	}
	if($size['mime'] != "image/jpeg")
	{
		echo "<script type=text/javascript>parent.location= \"javascript:result('101');\"</script>";
		unlink( $images_dir . $new_file_name .".jpg");
		return;
	}
	//OK!
	ini_set('memory_limit', '512M');
	$im = ImageCreateFromJPEG($images_dir . $new_file_name .".jpg");
	$rgb = imagecolorat($im, 0, 0);
	$r = ($rgb >> 16) & 0xFF;
	$g = ($rgb >> 8) & 0xFF;
	$b = $rgb & 0xFF;
	$rgb = "rgb($r, $g, $b)";
	ImageDestroy($im);
	if(1)
	{
		AddPicture($new_file_name .".jpg", $alt, $rgb, $size[0], $size[1]);
		echo "<script type=text/javascript>parent.location= \"javascript:result('200', '$new_file_name.jpg')\";</script>";
	}
 
 }else{
 echo "$file_name | could not be uploaded!<br>";
 }
?>