<?php
include("cnf.php");
require_once("db_cat.php");
function AddFav($fbid, $cat_id)
{
	if(!is_numeric($cat_id)) {return 0;}
	if(!GetCat($cat_id)) return 0;
	$fbid = addslashes($fbid);
	
	$res = $GLOBALS['db']->query("insert into visitor_and_cat(`visitor_id`, `cat_id`) values('$fbid', '$cat_id')");
	if(!$res) { return 0; }	
	return $GLOBALS['db']->insert_id;
}
function DeleteAllFavs($fbid)
{
	$fbid = addslashes($fbid);
	$res = $GLOBALS['db']->query("delete from visitor_and_cat where `visitor_id` = '$fbid'");
	if($res) return 1;
	return 0;
}
function AddVisitor($fbid, $name, $gender)
{
	$fbid = addslashes($fbid);
	$date = date("Y-m-d H:i:s");
	$res = $GLOBALS['db']->query("select id from visitor where fbid = '$fbid'");
	if($res && $res->num_rows) return 1;	
	$res = $GLOBALS['db']->query("insert into visitor(`fbid`, `name`, `date`,`gender`) values('$fbid', '$name', '$date', '$gender')");
	if($res) return 1;
	return 0;
}
function getUsersByCat($cat_id)
{
	if(!is_numeric($cat_id)) {return 0;}
	$res = $GLOBALS['db']->query("select visitor_id from visitor_and_cat where cat_id = '$cat_id'");
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function getVisitor($fbid)
{
	$fbid = addslashes($fbid);
	$res = $GLOBALS['db']->query("select * from visitor where fbid = '$fbid'");
	$row = $res->fetch_assoc();
	if($res && $row) return $row;
	return 0;
}
function AddAllFavs($fbid, $cats, $name, $gender)
{
	if(!AddVisitor($fbid, $name, $gender)) return 0;	
	if(!DeleteAllFavs($fbid)) return 0;
	$cats = explode(',', $cats);
	foreach($cats as $c)
	{
		if(is_numeric($c))
			AddFav($fbid, $c);
	}
	return 1;
}
?>
