<?php
function AddUpdate($table, $action, $item_id)
{
	if(strlen($table) < 2 || strlen($action) < 2)  { return 0; } 	

	$res = $GLOBALS['db']->query("insert into updates(`table`, `action`, `item_id`) values('$table', '$action', '$item_id')");
	if(!$res) { return 0; }	
	return 1;
}

function GetUpdate($id)
{
	if(!is_numeric($id)) { return 0;}
	$res = $GLOBALS['db']->query("select * from updates where id = '$id'");
	$row = $res->fetch_assoc();
	if(!$row) {return 0;}

	return $row;
}
function GetAllUpdates()
{
	$result = Array();
	$res = $GLOBALS['db']->query("select * from updates");
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		$result[$i] = $row;
		$i++;
	}	
	if($i == 0) return 0;
	return $result;
}
function GetAllUpdatesFrom($minId)
{
	if(!is_numeric($minId)) { return 0; }
	$res = $GLOBALS['db']->query("select * from updates where id  >= '$minId' limit 30");
	$arr = Array();
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		$arr[$i++] = $row;
	}
	return $arr;
}
?>