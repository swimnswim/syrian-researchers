<?php
include("cnf.php");
include("db_pic.php");
include("/home/syrresco/public_html/admin_cp_tt/admin_new/login/db.php");
ini_set('memory_limit', '128M');
if(!isLoggedIn())
{
	echo "<center>Sorry but you do not have access here, you have to log in first :)</center>";
	return;
}
/*
100: not an image
101: not a jpeg
102: bad size
200: OK
*/
$images_dir = "/home/syrresco/public_html/pictures/";


$file_name = $_FILES['uploadFile']['name'];
$rename = false; //rename file if already exists
$alt = $_POST['alt'];
if($alt == "") $alt = "Syrian Researchers";

$random = substr(md5(rand()),0, rand(5,15));

// strip file_name of slashes
$file_name = stripslashes($file_name);
$file_name = $random.base64_encode($file_name);
$new_file_name = $file_name;
$i = 2;
while(file_exists($images_dir . $new_file_name))
{
	$rename = true;
	$new_file_name = $i . '_' . $file_name;
	$i++;
}
$copy = copy($_FILES['uploadFile']['tmp_name'], $images_dir . $new_file_name .".jpg");

 // check if successfully copied
 if($copy){
	$size = getimagesize( $images_dir . $new_file_name .".jpg"); // replace filename with $_GET['src'] 
	if(!$size)
	{
		echo "<script type=text/javascript>parent.location= \"javascript:result('100');\"</script>";
		unlink( $images_dir . $new_file_name .".jpg");
		return;	
	}
	if($size['mime'] != "image/jpeg")
	{
		echo "<script type=text/javascript>parent.location= \"javascript:result('101');\"</script>";
		unlink( $images_dir . $new_file_name .".jpg");
		return;
	}
	if($size[0] != 900 || $size[1] != 180)
	{
		echo "<script type=text/javascript>parent.location= \"javascript:result('102');\"</script>";
		unlink( $images_dir . $new_file_name .".jpg");
		return;
	}
	//OK!
	$im = ImageCreateFromJPEG($images_dir . $new_file_name .".jpg");
	$rgb = imagecolorat($im, 0, 0);
	$r = ($rgb >> 16) & 0xFF;
	$g = ($rgb >> 8) & 0xFF;
	$b = $rgb & 0xFF;
	$rgb = "rgb($r, $g, $b)";
	ImageDestroy($im);
	if(1)
	{
		AddPicture($new_file_name .".jpg", $alt, $rgb, $size[0], $size[1]);
		echo "<script type=text/javascript>parent.location= \"javascript:result('200', '$new_file_name.jpg')\";</script>";
	}
 
 }else{
 echo "$file_name | could not be uploaded!<br>";
 }
?>