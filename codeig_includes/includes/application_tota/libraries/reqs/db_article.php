<?php
require_once("db_updates.php");

function AddArticle($cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $publish_date, $notify = 1, $audio_url = "", $isHTML = false)
{
	if(!is_numeric($cat_id)) {return -5;}
	if(!is_numeric($user_id)) {return -4;}
	if(!is_numeric($home_pic_id)) {return -3;}
	$isHTML = ($isHTML) ? 1 : 0;
	
	if(strlen($title) < 3) { return -2;}
	$date = date("Y-m-d H:i:s"); 
	$title = str_replace('\'', '&#39;', $title);

	$audio_url =  $GLOBALS['db']->escape_string($audio_url);
	$body  =  $GLOBALS['db']->escape_string($body);
	
	$q = "insert into article(`sub_cat_id`, `cat_id`, `user_id`, `title`, `body`, `date`, `home_pic_id`, `publish_date`, `notify`, `audio_url`,`html`) VALUES('$sub_cat_id', '$cat_id', '$user_id', '$title','$body', '$date', '$home_pic_id', '$publish_date', '$notify', '$audio_url', '$isHTML')";	
	if(!$result = $GLOBALS['db']->query($q))
	{
		echo $GLOBALS['db']->error;
		return 0;
		
	}
	return $GLOBALS['db']->insert_id;
}
function AddVideo($cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $publish_date, $notify = 1, $youtube_id)
{
	if(!is_numeric($cat_id)) {return -5;}
	if(!is_numeric($user_id)) {return -4;}
	if(!is_numeric($home_pic_id)) {return -3;}

	if(strlen($title) < 3) { return -2;}
	$date = date("Y-m-d H:i:s"); 
	$title = str_replace('\'', '&#39;', $title);
	$body  = str_replace('\'', '&#39;', $body);
	$body = nl2br($body);
	$audio_url =  $GLOBALS['db']->escape_string($audio_url);
	
	$q = "insert into article(`sub_cat_id`, `cat_id`, `user_id`, `title`, `body`, `date`, `home_pic_id`, `publish_date`, `notify`, `youtube_id`) VALUES('$sub_cat_id', '$cat_id', '$user_id', '$title','$body', '$date', '$home_pic_id', '$publish_date', '$notify', '$youtube_id')";	
	if(!$result = $GLOBALS['db']->query($q))
	{
		echo $GLOBALS['db']->error;
		return 0;		
	}
	return $GLOBALS['db']->insert_id;
}
function AddArticleWithFBID($cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $added_by, $fbid)
{
	if(!is_numeric($cat_id)) {return 0;}
	if(!is_numeric($user_id)) {return 0;}
	if(!is_numeric($home_pic_id)) {return 0;}

	if(strlen($title) < 3) { return 0;}
	if(strlen($body) < 10) { return 0;}
	$date = date("Y-m-d H:i:s");
	$title = str_replace('\'', '&#39;', $title);
	$body  = str_replace('\'', '&#39;', $body);
	
	$q = "insert into article(`sub_cat_id`, `cat_id`, `user_id`, `title`, `body`, `date`, `home_pic_id`, `added_by`, `fbid`) VALUES('$sub_cat_id', '$cat_id', '$user_id', '$title','$body', '$date', '$home_pic_id', '$added_by', '$fbid')";	
	
	if(!$result = $GLOBALS['db']->query($q))
		return 0;
		
	return $GLOBALS['db']->insert_id;
}
function incrementViews($id)
{
	if(!is_numeric($id)) { return 0; } 

	$GLOBALS['db']->query("update article set views=views+1 where id = '$id'");
	$result = $GLOBALS['db']->query("select * from article where id = '$id'");
	if(!$result)
	{ return 0;	}
	if($result->num_rows == 0) { return 0;}
	$arr = $result->fetch_assoc();
	return $arr['views'];
}
function set_facebook_shares($id, $shares)
{
	if(!is_numeric($id)) { return 0;}
	if(!is_numeric($shares)) { return 0;}
	
	$GLOBALS['db']->query("update article set fb_share_count='$shares' where id = '$id'");
}
function setFBID($id, $fbid)
{
	if(!is_numeric($id)) { return 0; }
	$fbid = $GLOBALS['db']->escape_string($fbid);	
	if($GLOBALS['db']->query("update article set `fbid` = '$fbid' where id = '$id'"))
		return 1;
	return 0;
}
function UpdateArticle($id, $cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $publish_date, $audio_url, $isHTML)
{
	if(!is_numeric($id)) { return 0;}
	if(!is_numeric($cat_id)) { return 0;}
	if(!is_numeric($user_id)) { echo "Bad author id..."; return 0;}
	if(!is_numeric($home_pic_id)) { echo "Bad home_pic_id..."; return 0;}
	$isHTML = ($isHTML) ? 1 : 0;
	
	if(strlen($title) < 3) { echo "Bad title (3) chars min"; return 0;}
	$date = date("Y-m-d H:i:s");
	$title = str_replace('\'', '&#39;', $title);
	
	$body  =  $GLOBALS['db']->escape_string($body);
	$audio_url =  $GLOBALS['db']->escape_string($audio_url);
	$publish_date =  $GLOBALS['db']->escape_string($publish_date);
	
	$q = "update article set `html` = '$isHTML', `audio_url` = '$audio_url', `sub_cat_id`='$sub_cat_id', `cat_id`='$cat_id', `user_id`='$user_id', `title`='$title', `body`='$body', `home_pic_id`='$home_pic_id', `publish_date` = '$publish_date' where id = '$id'";	

	if(!$result = $GLOBALS['db']->query($q))
		return -1;
	
	AddUpdate("article", "update", $id);
	return $id;
}
function UpdateVideo($id, $cat_id, $sub_cat_id, $user_id, $title, $body, $home_pic_id, $publish_date, $youtube_id)
{
	if(!is_numeric($id)) { return 0;}
	if(!is_numeric($cat_id)) { return 0;}
	if(!is_numeric($user_id)) { echo "Bad author id..."; return 0;}
	if(!is_numeric($home_pic_id)) { echo "Bad home_pic_id..."; return 0;}
	
	if(strlen($title) < 3) { echo "Bad title (3) chars min"; return 0;}
	$date = date("Y-m-d H:i:s");
	$title = str_replace('\'', '&#39;', $title);
	$body  = str_replace('\'', '&#39;', $body);
	$body =  $GLOBALS['db']->escape_string($body);
	$body = nl2br($body);
	$q = "update article set `youtube_id` = '$youtube_id', `sub_cat_id`='$sub_cat_id', `cat_id`='$cat_id', `user_id`='$user_id', `title`='$title', `body`='$body', `home_pic_id`='$home_pic_id', `publish_date` = '$publish_date' where id = '$id'";	
	
	if(!$result = $GLOBALS['db']->query($q))
		return -1;
	
	AddUpdate("article", "update", $id);
	return $id;
}
function DeleteArticle($id)
{
	if(!is_numeric($id)) { echo "Error: Bad id..."; return 0;}
	if(!$result = $GLOBALS['db']->query("update article set `deleted` = '1' where id = '$id'"))
		{ echo "Error: an error occured while deleting article..."; return 0;}
	AddUpdate("article", "delete", $id);
	return 1;
}

function GetArticlesByPublishDate($date)
{
	$result = Array(); 	
	$res = $GLOBALS['db']->query("SELECT id, title FROM article where deleted = 0 and  ABS(UNIX_TIMESTAMP(publish_date) - UNIX_TIMESTAMP('$date')) <= 1700");

	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $row['title'] = str_replace("<3", "♥", $row['title']);
	   $result[$i++] = $row;
	}
	return $result;
}

function addArticleAndTag($art_id, $tag_id)
{
	$art_id = $GLOBALS['db']->escape_string($art_id);
	$tag_id = $GLOBALS['db']->escape_string($tag_id);
	
	$GLOBALS['db']->query("insert into article_and_tag(`article_id`, `tag_id`) values('$art_id', '$tag_id')");
}
function UpdateArticleRow($id, $field, $value)
{
	$value = addslashes($value);
	if(!is_numeric($id)) { echo "Bad id ($id)..."; return 0;}
	if(strlen($field) < 2) { echo "Bad field name"; return 0;}
	if(!$result = $GLOBALS['db']->query("update article set `$field` = '$value' where id = '$id'"))
		{ echo "Error updating article..."; return 0;}
	AddUpdate("article", "update", $id);
	return 1;
}
function GetArticleById($id)
{	
	if(!is_numeric($id)) { return 0;}
	$result = $GLOBALS['db']->query("select *  from article where id = '$id' and deleted = 0");
	if(!$result)
	{	echo "No such article..."; return 0;	}
	if($result->num_rows == 0) { return 0;}
	$arr = $result->fetch_assoc();
	$arr['body'] = str_replace("<3", "♥", $arr['body']);
	$arr['title'] = str_replace("<3", "♥", $arr['title']);
	return $arr;
}
function GetLastVideo()
{	
	$result = $GLOBALS['db']->query("select *  from article where deleted = 0 and youtube_id != '0' order by id DESC limit 1");
	if(!$result)
	{ return 0;	}
	if($result->num_rows == 0) { return 0;}
	$arr = $result->fetch_assoc();
	$arr['body'] = str_replace("<3", "♥", $arr['body']);
	$arr['title'] = str_replace("<3", "♥", $arr['title']);
	return $arr;
}
function GetArticleByFBID($fbid)
{	
	if($fbid == "") return 0;
	$fbid = $GLOBALS['db']->escape_string($fbid);
	$result = $GLOBALS['db']->query("select * from article where `fbid` = '$fbid' and deleted = 0 and article.youtube_id = 0");
	if(!$result)
	{ return 0;	}
	
	if($result->num_rows == 0) { return 0;}
	$arr = $result->fetch_assoc();

	return $arr;
}
function GetAllArticleForStats($page)
{	
	if($page == "") $page = 1;
	$page--;
	$start = $page * 50;
	
	$res = $GLOBALS['db']->query("select fbid,id,user_id,views,fb_share_count from article where LOCATE('_', fbid) > 0 and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by ID DESC limit $start, 50");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	$result = Array();	
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllArticles($page)
{
	if($page == "") $page = 1;
	$page--;
	$start = $page * 20;
	$result = Array();
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = '0' and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC limit $start, 20");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllVideos($page)
{
	if($page == "") $page = 1;
	$page--;
	$start = $page * 20;
	$result = Array();
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and youtube_id != '0' and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC limit $start, 20");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllAllVideos()
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and youtube_id != '0'  order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetNextVideos($id)
{
	$result = Array();
	$id = $GLOBALS['db']->escape_string($id);
	$res = $GLOBALS['db']->query("SELECT `id`, title, youtube_id, views FROM article where `id` <= $id and deleted = 0 and youtube_id != '0' and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC limit 20");
	
	if($res->num_rows == 0) { return -1;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return json_encode($result);
}
function GetRelatedArticles($id)
{
	if(!is_numeric($id)) { return 0;}
	$result = Array();
	
	$res = $GLOBALS['db']->query("SELECT article.id, article.title, article.home_pic_id, article.sub_cat_id FROM article INNER JOIN article_and_tag ON article_and_tag.article_id = article.id where article.id != '$id' and article.deleted = 0 and article.youtube_id = 0 and article.publish_date <= '".date("Y-m-d H:i:s")."' and article_and_tag.tag_id IN (select tag_id from article_and_tag where article_id = '$id') group by article_id limit 5");
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	$limit = 10 - $i;
	if($i < 6)
	{
		$res = $GLOBALS['db']->query("SELECT * from article where id != $id and deleted = 0 and article.youtube_id = 0 and sub_cat_id IN (select sub_cat_id from article where id = '$id') limit $limit");
		while($row = $res->fetch_assoc())
		{  
			$result[$i++] = $row;
		}	
	}
	if($i == 0) return 0;
	return $result;
}
function GetHistoryArticles($id)
{
	$result = Array();
	$ldate = date("Y-m-d H:i:s", $id);
	$q = "SELECT * FROM article where id < '$id' and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC limit 20";
	$res = $GLOBALS['db']->query($q);
	if($res->num_rows == 0) { return 0; }
	$i = 0;

	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllArticlesMobile($page, $lastPD)
{
	if($page == "") $page = 1;
	if(!is_numeric($lastPD)) return;
	$page--;
	$start = $page * 20;
	$result = Array();
	$res = $GLOBALS['db']->query("SELECT * FROM article where id > '$lastPD' and html = 0 and home_pic_id != 0 and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by DATE(publish_date) DESC limit $start, 20");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{  
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllArticlesDated($page)
{
	if($page == "") $page = 1;
	$page--;
	$start = $page * 20;
	$result = Array(); 
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by DATE(publish_date) DESC limit $start, 20");

	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $row['body'] = str_replace("<3", "♥", $row['body']);
	   $row['title'] = str_replace("<3", "♥", $row['title']);
	   $result[$i++] = $row;
	}
	return $result;
}
function ArticlesToNotify()
{
	$result = Array(); 	
	$res = $GLOBALS['db']->query("SELECT * FROM article where notify = 1 and notified = 0 and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by DATE(publish_date) DESC");
	
	$num_rows = $res->num_rows;
	if($num_rows == 0) return 0;
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	$res = $GLOBALS['db']->query("update article set notified = 1 where notify = 1 and notified = 0 and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."'");
	if($num_rows == 0) return 0;
	return $result;
}
function GetScheduledArticles()
{
	$result = Array(); 	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 and publish_date >= '".date("Y-m-d H:i:s")."' order by DATE(publish_date) DESC");
	
	$num_rows = $res->num_rows;
	if($num_rows == 0) return 0;
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetCoverArticles()
{
	$result = Array();
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by rand() limit 5");

	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetHistoryArticlesBySubcat($id, $cat_id)
{
	$result = Array();
	$q = "SELECT * FROM article where id < $id and publish_date <= '".date("Y-m-d H:i:s")."' and deleted = 0 and article.youtube_id = 0 and sub_cat_id = $cat_id order by id DESC limit 20";
	$res = $GLOBALS['db']->query($q);
	if($res->num_rows == 0) { return 0; }
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllAllArticles()
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesToUpdate()
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 order by id DESC limit 500");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}

function GetTotalArticleViews()
{
	$res = $GLOBALS['db']->query("SELECT SUM(views) as s FROM article where deleted < 1");
	
	if($res->num_rows == 0) { return 0;}
	$row = $res->fetch_assoc();
	return $row;
}
function MaxArticleViews()
{
	$res = $GLOBALS['db']->query("SELECT id, title, views FROM article where deleted < 1 order by views DESC limit 1");
	
	if($res->num_rows == 0) { return 0;}
	$row = $res->fetch_assoc();
	return $row;
}
function MinArticleViews()
{
	$res = $GLOBALS['db']->query("SELECT id, title, views FROM article where deleted < 1 order by views ASC limit 1");
	
	if($res->num_rows == 0) { return 0;}
	$row = $res->fetch_assoc();
	return $row;
}
function GetAllArticlesByUser2($id)
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 and user_id = '$id' order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetAllArticlesByUserDated($id)
{
	$result = Array();	
	$res = $GLOBALS['db']->query("SELECT * FROM article where deleted = 0 and article.youtube_id = 0 and user_id = '$id' and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesByTagId2($tag_id)
{
	$result = Array();
	if(!is_numeric($tag_id)) { echo "Bad tag id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article INNER JOIN article_and_tag ON article_and_tag.article_id = article.id where tag_id='$tag_id' and article.deleted = 0 and article.youtube_id = 0 group by article_id");
	echo $GLOBALS['db']->error;
	if($res->num_rows == 0) { return -1;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesByTagIdDated($tag_id)
{
	$result = Array();
	if(!is_numeric($tag_id)) { echo "Bad tag id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article INNER JOIN article_and_tag ON article_and_tag.article_id = article.id where tag_id='$tag_id' and article.deleted = 0 and article.youtube_id = 0 and article.publish_date <= '".date("Y-m-d H:i:s")."' group by article_id");
	echo $GLOBALS['db']->error;
	if($res->num_rows == 0) { return -1;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesByCatId2($cat_id)
{
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad cat id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 ");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesByCatIdForStats($cat_id, $start_date, $end_date, $all)
{
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad cat id..."; return 0;}	
	
	$res = null;
	if($all)
		$res = $GLOBALS['db']->query("SELECT * FROM article where cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0");
	else
		$res = $GLOBALS['db']->query("SELECT * FROM article where cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 and DATE(`date`) >= DATE('$start_date') and DATE(`date`) <= DATE('$end_date')");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
		$stat = GetStat($row[id]);
		$stat['local_views'] = $row['views'];
		$row['stats'] = $stat;
	    $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesByCatIdPage2($cat_id, $page)
{
	if($page == "") $page = 1;
	$page--;
	$start = $page * 20;
	
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad cat id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 limit $start, 20 ");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $array;
	}
	return $result;
}
function GetArticlesBySubCatId2($cat_id)
{
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad sub cat id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where sub_cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesBySubCatIdDated($cat_id)
{
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad sub cat id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where sub_cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $row['body'] = str_replace("<3", "♥", $row['body']);
	   $row['title'] = str_replace("<3", "♥", $row['title']);
	   $result[$i++] = $row;
	}
	return $result;
}

function GetArticlesBySubCatIdPage2($cat_id, $page = 1)
{
	$page--;
	$start = $page * 20;
	
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad sub cat id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where sub_cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 order by id DESC limit $start, 20");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
function GetArticlesBySubCatIdPageDated($cat_id, $page = 1)
{
	$page--;
	$start = $page * 20;
	
	$result = Array();
	if(!is_numeric($cat_id)) { echo "Bad sub cat id..."; return 0;}	
	
	$res = $GLOBALS['db']->query("SELECT * FROM article where sub_cat_id='$cat_id' and deleted = 0 and article.youtube_id = 0 and publish_date <= '".date("Y-m-d H:i:s")."' order by id DESC limit $start, 20");
	
	if($res->num_rows == 0) { return 0;}
	$i = 0;
	while($row = $res->fetch_assoc())
	{
	   $result[$i++] = $row;
	}
	return $result;
}
