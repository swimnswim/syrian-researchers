if(isInfog || referrer.indexOf('facebook') < 0)
{
	$('#article-img').attr('src', $('#article-img').attr('data-src'));
	var width = $('#article-img').width();			
	var height = width * aspectRatio;
	$('#article-img').height(height);
}
else
{
	var width = $('#article-img').width();			
	var height = width * aspectRatio;
	$('#article-img').height(height);
	$('#article-img').css('cursor', 'pointer');
	$('#article-img').css('background-image', "url('" + home_url + 'images/unloaded.png' + "')");
	$('#article-img').click(function()
	{
		$(this).attr('src', $(this).attr('data-src'));
		$('#article-img').css('cursor', 'default');
	});
}		
$.get(home_url + 'article/view/' + article_id + '.json');
var ratingScores = new Object();
ratingScores.done = false;
$('.rating span').click(function()
{
	var index = 4 - $(this).index();
	var value = 5 - $(this).index();
	
	eval("ratingScores." + $(this).parent().attr('id') +" = " + value);

	for(var i = 0; i < 5;i++)
	{
		$(this).parent().children()[i].innerHTML = '☆';
	}
	for(var i = 4; i > 4 - value;i--)
	{
		$(this).parent().children()[i].innerHTML = '★';
	}
	if(!ratingScores.done && ratingScores.spelling && ratingScores.accuracy && ratingScores.language && ratingScores.writing)
	{
		ratingScores.done = true;
		$('#rating-button').show('slow');
		$('#rating-button').click(submitRating);
	}
});
function submitRating()
{
	ratingScores.token = FB.getAccessToken();
	$('#quality-div').css('opacity', '0.5');
	$.post(home_url + 'article/rate/' + article_id + '.json', "rating=" + JSON.stringify(ratingScores), function(data)
	{
		if(data == 1)
			$('#quality-div').html('<h3>لقد ساهمت في تطوير العمل.. شكراً لك</h3>');
		else
			$('#quality-div').html('<h3>لقد قمت بالتقييم مسبقاً.. شكراً على كل حال</h3>');
	});			
}
$(document).scroll(function()
{
	try
	{
		if(!read && $(window).scrollTop() + $(window).height() > $('#end-div').offset().top)
		{
			read = true;
			$(document).scroll();
			$.get(home_url + 'article/read/' + article_id + '.json');
		}
	}
	catch(ex){}	
	if($(window).width() > 600 && $('#end-div').length > 0) // $('#end-div').length > 0) only on article page
	{
		if($(window).scrollTop() >= $(window).height() - 500) // scrolled one page
		{
			reachedRelated = true;
		}		
		if(!gotRelated && reachedRelated && lastScroll > $(window).scrollTop() && $(window).scrollTop() < 200) // going back up
		{
			getRelated(cat_id);
			gotRelated = true;
		}
		lastScroll = $(window).scrollTop();
	}
});
window.fbAsyncInit = function()
{
	try
	{
		FB.init({
		appId      : '496625687093064',
		xfbml      : true,
		version    : 'v2.3'
		});
		handler = function(response) {
			
			// Here we specify what we do with the response anytime this event occurs. 
			if (response.status === 'connected')
			{
				FB.api("/me", function(res)	
				{ 		
					$.post(home_url + "article/validate/", "unique_id="+FB.getAccessToken(), function(data)
					{
						if(data == res.id)
						{
							$('#edit-div').show();							
							//$('#quality-div').show();								
							$('#edit-div a').click(function() {window.location.href = 'edit/' + article_id + '.html';});							
						}
					});
				});
			}
		};
		FB.getLoginStatus(handler);
	}
	catch(e){}
}
window.fbAsyncInit();
$(document).ready(function()
{
	var isIos = navigator.userAgent.match(/Android|iPhone/i);
	if(isIos)
	{ 
		$('#wa-btn').css('display', 'inline-block');
		$('#fb-share-btn').width('51px');
	}			
});
