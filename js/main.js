var defaultCacheTime = 1000;
var catchNavigation = true;
var lastAddress = "";
var navbarOffset;
var referrer = document.referrer;
var read = false;
var reachedRelated = false;
var lastScroll = 0;
var gotRelated = false;

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ar_AR/sdk.js#xfbml=1&appId=496625687093064&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


	var catsLoaded = false;
	function showMenu()
	{
		if(!$('#collapse-menu-btn').is(':hidden')) $('#cats-menu').hide('slow');
		$('#menu-bar').toggle('slow');
	}
	function showCatsMenu()
	{
		getCats();
		if(!$('#collapse-menu-btn').is(':hidden')) $('#menu-bar').hide('slow');
		$('#cats-menu').toggle('slow');
	}
	function close_cats_menu()
	{
		$('#cats-menu').hide('slow');
	}
	function close_main_menu()
	{
		if(!$('#collapse-menu-btn').is(':hidden')) $('#menu-bar').hide('slow')
	}	
	function renderCat(c)
	{
		var cat = '<div class="cat-item"><h4>'+c.name+'</h4><ul class="subcats-ul">'
		for(var i = 0 ; i < c.subcats.length;i++)
			cat += '<li><a data-asset="'+ home_url + 'subcat/' +  c.subcats[i].id + '.json" href="' + home_url + 'subcat/' + c.subcats[i].id + '.html">'+c.subcats[i].name+'</a></li>';
		cat += '</ul></div>';
		return $(cat);
	}
	function parseCats(data)
	{
		$('#cats_div').html('');
		for(var j=0;j<data.length;j++)
			$('#cats_div').append(renderCat(data[j]));
		ParseNewTags();		
	}
	function dataBank(url, callback)
	{
		var currentTime = new Date().getTime();
		if(localStorage && localStorage.getItem(url) && localStorage.getItem(url + '_time') && currentTime - parseInt(localStorage.getItem(url + '_time')) < defaultCacheTime) // 30 mins default cache time
		{
			callback(localStorage.getItem(url));
		}
		else
		{
			$.get(url, function(data)
			{
				saveToBack(url, data);
				callback(data);
			}			
			);
		}
	}
	function saveToBack(url, data)
	{
		var currentTime = new Date().getTime();
		localStorage.setItem(url, data);
		localStorage.setItem(url + '_time', currentTime)
	}
	function catchHrefClick()
	{
		var el = $(this);
		if(el.attr('data-asset'))
		{			
			getBody(el.attr('data-asset'));
			read = true;
			return false;
		}			
	}
	function CleanRelated()
	{
		$('.related-c-item').remove();
		$('#related-articles').hide('slow');
	}
	function ParseNewTags()
	{
		close_cats_menu();
		close_main_menu();
		$('a').click(catchHrefClick);		
		FB.XFBML.parse();
		$('#fb-share-btn').click(function()
		{
			FB.ui({
				  method: 'share',
				  href: window.location.href,
			}, function(response){});			
		});
		$('#gp-share-btn').click(function()
		{
			popupwindow("https://plus.google.com/share?url="+escape(window.location.href), 'Share on G+', 520, 570);
		});
		$('#pin-share-btn').click(function()
		{
			popupwindow("http://pinterest.com/pin/create/button/?url="+escape(window.location.href), 'Pin it', 700, 550);
		});	
		$('#tweet-share-btn').click(function()
		{
			popupwindow("http://twitter.com/share?url="+escape(window.location.href)+"&text=" + escape("مشاركة من الباحثون السوريون"), 'Pin it', 700, 550);
		});	
	}
	function PushToHistory(navObj)
	{
		history.pushState(navObj, "", navObj.url);
	}
	function showLoading(a)
	{
		if(a)
		{
			$('#loading-div').show();
			$('#loading-div').animate({width: '100%'}, 10000);			
		}
		else
		{
			$('#loading-div').show();
			$('#loading-div').stop(false, true);
			$('#loading-div').css('width', '0%');
		}
	}
	function getBody(url)
	{		
		referrer = '';
		if(url == lastAddress) return;
		showLoading(true);
		var historyObj = { type: 'not_home', 'url': url.replace('json', 'html').replace('ajax','') };		
		$.get(url.replace('html', 'json'), function(data)
		{
			CleanRelated();
			showLoading(false);
			if(url == lastAddress) return;
			lastAddress = url;
			$('#main-section').html(data);
			PushToHistory(historyObj);
			$('#return-to-top').click();
			setTimeout("read = false;", 500);
			ParseNewTags();			
		});	
		return false;
	}	
	function renderRelated(obj)
	{		
		var div = $('<div class="related-c-item"><a data-asset="'+ home_url + 'article/' + obj.article_id + '.json" href="'+ home_url + 'article/' + obj.article_id + '.html"><img src="http://www.syr-res.com/pic_ret.php?id='+obj.home_pic_id+'"><h4>'+obj.title+'</h4></a></div>');
		return div;
	}
	function getRelated(cat_id)
	{
		$.getJSON(home_url + 'article/related/' + cat_id, function(data)
		{
			for(var i = 0; i < data.length; i++)
				$('#related-holder').append(renderRelated(data[i]));
			$('#related-articles').show('slow');
		});	
	}	
	function getCats()
	{
		if(catsLoaded) return;
		var data;
		var currentTime = new Date().getTime();
		if(typeof(Storage) !== "undefined")
		{
			var time = false; //localStorage.getItem("cats_json_time");
			if(time)
				if(currentTime - parseInt(time) < 10800000) // update cats every 3 hours
					data = localStorage.getItem("cats_json");		
		}
		if(data)
		{
			parseCats(JSON.parse(data));
		}
		else
		{
			$.getJSON(home_url + 'cats_ajax/', function(data)
			{
				parseCats(data);
				localStorage.setItem("cats_json", JSON.stringify(data));
				localStorage.setItem("cats_json_time", currentTime);
				catsLoaded = true;
			});
		}
	}
	function notifyCats()
	{
		loop = 0;
		var interval = setInterval(function()
		{
			if(loop % 2 == 0)
				$('.hint--bottom').addClass('hovered');
			else
				$('.hint--bottom').removeClass('hovered');
			if(loop == 4)
			{
				clearInterval(interval);
			}
			loop++;
		}, 500);
		setTimeout(function()
		{
			$('.hint--bottom').removeClass('hovered');
			setCookie('seen_cats', 1, 365);
		}, 9000);	
	}
	$(window).scroll(function() {
	    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
		   $('#return-to-top').fadeIn(200);    // Fade in the arrow
	    } else {
		   $('#return-to-top').fadeOut(200);   // Else fade out the arrow
	    }
	    if($(this).scrollTop() >= navbarOffset)
	    {
			$('#menu-big-container').addClass('fixed-navbar');
	    }
	    else
	    {
		$('#menu-big-container').removeClass('fixed-navbar');
	    }	
	});
	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires;
	}
	function popupwindow(url,title,w,h){var left=(screen.width/2)-(w/2);var top=(screen.height/2)-(h/2);return window.open(url,title,'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=auto, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);}
	$(document).ready(function()
	{
	
		
		
		//make navbar fixed when scrolled down
		navbarOffset = $('#menu-big-container').offset().top;
		$('#fb-share-btn').click(function()
		{
			FB.ui({
				  method: 'share',
				  href: window.location.href,
			}, function(response){});			
		});
		//end
		
		//attach share buttons events
		$('#gp-share-btn').click(function()
		{
			popupwindow("https://plus.google.com/share?url="+escape(window.location.href), 'Share on G+', 520, 570);
		});
		$('#pin-share-btn').click(function()
		{
			popupwindow("http://pinterest.com/pin/create/button/?url="+escape(window.location.href), 'Pin it', 700, 550);
		});	
		$('#tweet-share-btn').click(function()
		{
			popupwindow("http://twitter.com/share?url="+escape(window.location.href)+"&text=" + escape("مشاركة من الباحثون السوريون"), 'Pin it', 700, 550);
		});	
		//end
		
		$('#related-more').click(function()
		{
			CleanRelated();
			getRelated(cat_id);		
		});
		$('#return-to-top').click(function() {      // When arrow is clicked
		    $('body,html').animate({
			   scrollTop : 0                       // Scroll to top of body
		    }, 500);
		});
		// catch navigation
		if(catchNavigation)
		{
			$('a').click(catchHrefClick);				
			window.onpopstate = function(data)
			{
				console.log(data);
				if(data.state)
				{				
					if(data.state.url == home_url)
					{
						getBody(home_url + 'ajax');
					}
					else
					{
						getBody(data.state.url);
					}
				}
			}			
			var historyObj = { type: 'home', 'uid': '', 'url': window.location.href };
			PushToHistory(historyObj);	
		}
	});
