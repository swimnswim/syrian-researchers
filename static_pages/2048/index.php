<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>2048</title>

  <link href="style/main.css" rel="stylesheet" type="text/css">
  <link href="/css/omer.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" href="favicon.ico">
  <link rel="apple-touch-icon" href="meta/apple-touch-icon.png">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, maximum-scale=1, user-scalable=no, minimal-ui">
</head>
<body>
<script>
window.playSound = true;
  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '496625687093064',                        // App ID from the app dashboard
      channelUrl : 'http://syr-res.com/channel.html', // Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true                                // Look for social plugins on the page
    });
	
    // Additional initialization code such as adding Event Listeners goes here
  };
  
  // Load the SDK asynchronously
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/ar_AR/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   function Mute()
   {
		if(window.playSound)
		{
			document.getElementById('soundKey').src = 'no-sound.jpg';
			window.playSound = false;
		}
		else
		{
			document.getElementById('soundKey').src = 'sound.jpg';
			window.playSound = true;		
		}	
   }
 </script>
 
  <div class="container">
    <div class="heading">
      <h1 class="title">2048</h1>
      <div class="scores-container">
        <div class="score-container">0</div>
        <div class="best-container">0</div>
      </div>
    </div>
    <div class="above-game">
	  <p style="color: blue; cursor: pointer; font-size: 40pt" onclick="Mute()"><img  id=soundKey src="sound.jpg"></p>
      <p class="game-intro">ادمج الأرقام المتجاورة، للحصول على الرقم <strong>2048</strong></p><br />
      <p><a class="restart-button">بداية جديدة</a></p>
    </div>

    <div class="game-container">
      <div class="game-message">
        <p></p>
        <div class="lower">
	        <a class="keep-playing-button">استمر</a>
          <a class="retry-button">محاولة جديدة</a>
        </div>
      </div>

      <div class="grid-container">
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
      </div>

      <div class="tile-container">

      </div>
    </div>

    <p class="game-explanation">
      <strong class="important">طريقة اللعب:</strong>
		استخدم الأسهم على لوحة المفاتيح لتحريك المربعات بالإتجاه المطلوب. عندما يتلاقى مربعان لهما نفس الرقم يندمجان في مربع واحد وهو ناتج جمعهما! هل يمكنك الوصول لمربع الـ2048؟
    </p>
	<?php
	require_once("../reqs/db_article.php");	
	$vs = incrementViews(1709);	
	?>
	<div style='width: 100%;'>
		<p class=article_text dir=rtl align=right style="font-size: 18pt;"><span style="font-size: 18pt; color: #07a9f4"><?php echo $vs ?></span> شخص قام باللعب</p>
	</div>
	<div style="clear:both"></div>
	<p style="width: 280px;"><fb:like href="http://syr-res.com/2048.php" layout="standard" data-width="280" action="like" show_faces="false" share="false"></fb:like></p>
    <hr>
    <p class="game-info">
    Created by <a href="http://gabrielecirulli.com" target="_blank">Gabriele Cirulli.</a> Based on <a href="https://itunes.apple.com/us/app/1024!/id823499224" target="_blank">1024 by Veewo Studio</a> and conceptually similar to <a href="http://asherv.com/threes/" target="_blank">Threes by Asher Vollmer.</a> Translated by Omer Ahmed (SYR-RES)
    </p>
  </div>

  <script src="/2048/js/bind_polyfill.js"></script>
  <script src="/2048/js/classlist_polyfill.js"></script>
  <script src="/2048/js/animframe_polyfill.js"></script>
  <script src="/2048/js/keyboard_input_manager.js"></script>
  <script src="/2048/js/html_actuator.js"></script>
  <script src="/2048/js/grid.js"></script>
  <script src="/2048/js/tile.js"></script>
  <script src="/2048/js/local_storage_manager.js"></script>
  <script src="/2048/js/game_manager.js"></script>
  <script src="/2048/js/application.js"></script>
<audio id="player">
  <source src="move.mp3" type="audio/mpeg">
</audio> 
</body>
<?php
	include("/tracking.txt");
?>
</html>