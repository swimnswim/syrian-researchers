// Wait till the browser is ready to render the game (avoids glitches)
var _gameManager;
window.requestAnimationFrame(function () {
  _gameManager = new GameManager(4, KeyboardInputManager, HTMLActuator, LocalStorageManager);
});
